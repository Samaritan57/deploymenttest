﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using WorkAreaCreatorLib;

namespace UnitTestWorkAreaCreator
{
    [TestClass]
    public class Engine_FileManipulationHelperTesting : FileManipulationHelper
    {
        //Mainly used to clean repo after test runs
        private void CleanFiles(string [] filesPath )
        {
            foreach (var file in filesPath)
            {
                if (File.Exists(file))
                    File.Delete(file);
            }
        }
        /// <summary>
        /// Case_1, if everything is fine, nothing else should really happens. Nice to have,
        /// get some logs to notify the change
        /// 
        /// Case_2, Should overwrite the current doc when restarted without generating any exception
        /// 
        /// Case_3, try to copy a file wich is not existing --> FileNotFoundException 
        /// 
        /// Case_4, Try to copy a file to a location wich is not reachable (Cause the target file 
        /// is blocked by an other process) --> IOException
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_CopyFile()
        {
            //Set up a txt file to copy in order to test  
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string fileName = "TestText.txt";
            string fileName2 = "TestTextCopy.txt";

            string toCopy = Path.Combine(workingDirectory, fileName);
            string copyTo = Path.Combine(workingDirectory, fileName2);

            CleanFiles(new string[] { toCopy, copyTo });

            var myFile = File.CreateText(toCopy);
            myFile.Close();

            var myCreator = this;

            //Case_1, if everything is fine, nothing else should really happens.
            //Nice to have, get some logs to notify the change
            try
            {
                myCreator.CopyFile(toCopy, copyTo);
                Assert.IsTrue(File.Exists(copyTo), String.Format("Problem occured when try to copy the file from {0} to {1}", toCopy, copyTo));
                
            }catch (Exception e)
            {
                Assert.Fail(String.Format("Not able to copy the file, this excepton occured: \n{0}",e));
            }

            //Case_2, Should overwrite the current doc when restarted without generating any exception: 
            try
            {
                myCreator.CopyFile(toCopy, copyTo);
                Assert.IsTrue(File.Exists(copyTo), String.Format("Problem occured when try to copy the file from {0} to {1}", toCopy, copyTo));
            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Not able to copy the file, this excepton occured: \n{0}", e));
            }

            //Case_3, try to copy a file wich is not existing: 
            //The system should remark the error and report a File not found exception, this exception
            //should be then managed by the WorkArea builder object  
            CleanFiles(new string[] { toCopy, copyTo });

            Assert.ThrowsException<FileNotFoundException>(
                () => { myCreator.CopyFile(toCopy, copyTo); }
                , String.Format("The expected exception didn't occured!")) ;

            //Case_4, Try to copy a file to a location wich is not reachable
            //(Cause the target file is blocked by an other process)
            myFile = File.CreateText(toCopy);
            myFile.Close();
            var myFile2 = File.CreateText(copyTo);


            Assert.ThrowsException<IOException>(
                () => { myCreator.CopyFile(toCopy, copyTo); }
                , String.Format("The expected exception didn't occured!"));

            myFile2.Close();

            //Cleanup
            CleanFiles(new string[] { toCopy, copyTo });
            myCreator = null; 
        }

        /// <summary>
        /// Test if Zip to target directory works on differnet conditions
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_ZipFolderToTarget()
        {
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());

            //file that is contained in the directory
            string fileName = "TestText.txt";

            string directoryToZip = Path.Combine(workingDirectory, "DirectoryToZip");
            string fileInDirectory = Path.Combine(directoryToZip, fileName);
            string zip = Path.Combine(workingDirectory, "TestZip.zip");

            //Clean up
            if (Directory.Exists(directoryToZip))
                Directory.Delete(directoryToZip, true);

            if (File.Exists(zip))
                File.Delete(zip);

            //create the directory
            Directory.CreateDirectory(directoryToZip);

            //create the sample text file
            using (var file = File.CreateText(fileInDirectory)) file.Close();

            //case 1: check if the file is created succesfully
            var mycreator = this;
            mycreator.ZipFolderToTarget(directoryToZip, zip);

            Assert.IsTrue(File.Exists(zip), String.Format("Problem occured when try to zip the directory..."));

            
            //case 2: when given the same name for the zip, the zip must be overwritten
            DateTime time1 = File.GetCreationTime(zip);

            mycreator.ZipFolderToTarget(directoryToZip, zip);

            //check if zip exists and if it is overwritten based on time 
            Assert.IsTrue(File.Exists(zip), String.Format("Problem occured when try to zip the directory..."));

            DateTime time2 = File.GetCreationTime(zip);
            Assert.IsTrue(time1 <= time2, String.Format("Directory was not overwritten..."));

            //case 3: when given a wrong directory, the zip file must not be created()
            File.Delete(zip);

            mycreator.ZipFolderToTarget(Path.Combine(directoryToZip, "random"), zip);

            Assert.IsTrue(!File.Exists(zip), "Zip was somehow created");
        }

        /// <summary>
        /// test the unzipping proccess if it runs correctly
        /// we use the ZipFolderToTarget in order to test the unzip
        /// so it has to be tested before by the above test method
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_UnzipTargetDirectory()
        {
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());

            //file that is contained in the directory
            string fileName = "TestText.txt";

            string directoryToUnZip = Path.Combine(workingDirectory, "DirectoryToUnZip");
            string fileInDirectory = Path.Combine(directoryToUnZip, fileName);
            string zip = Path.Combine(workingDirectory, "TestZip.zip");

            //Clean up
            if (Directory.Exists(directoryToUnZip))
                Directory.Delete(directoryToUnZip, true);

            if (File.Exists(zip))
                File.Delete(zip);

            //create the directory
            Directory.CreateDirectory(directoryToUnZip);

            //create the sample text file
            using (var file = File.CreateText(fileInDirectory)) file.Close();

            //zip the directory
            var mycreator = this;
            mycreator.ZipFolderToTarget(directoryToUnZip, zip);

            //case 1: given the name of the zip file, unzip to desired location
            string unzipLocation = Path.Combine(workingDirectory, "UnzipedLocation");
            mycreator.UnzipTargetDirectory(zip, unzipLocation);

            Assert.IsTrue(Directory.Exists(unzipLocation));

            //case 2: when unziping to the same directory, changes must be overwritten
            DateTime time1 = File.GetCreationTime(zip);

            mycreator.UnzipTargetDirectory(zip, unzipLocation);

            //check if directory exists and if it is overwritten based on time 
            Assert.IsTrue(Directory.Exists(unzipLocation), String.Format("Problem occured when try to unzip the directory..."));

            DateTime time2 = File.GetCreationTime(zip);
            Assert.IsTrue(time1 <= time2, String.Format("Directory was not overwritten..."));

            //case 3: when zip has the wrong name, dont create the directory
            if (Directory.Exists(unzipLocation))
                Directory.Delete(unzipLocation, true);

            if (File.Exists(zip))
                File.Delete(zip);

            mycreator.UnzipTargetDirectory(zip, unzipLocation);

            Assert.IsTrue(!Directory.Exists(unzipLocation), String.Format("Directory was somehow created"));

        }

        /// <summary>
        /// Case_1, if everything is fine, nothing else should really happens. 
        /// Nice to have, get some logs to notify the change
        /// 
        /// Case_2, Should overwrite the current doc when restarted without generating any exception
        /// 
        /// Case_3, try to copy a file wich is not existing: --> DirectoryNotFoundException
        /// 
        /// Case_4, Try to copy a file to a location wich is not reachable (Cause the target file is 
        /// blocked by an other process) --> IOException
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_CopyFolder()
        {
            //Set up a txt file to copy in order to test
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string workDirectory = "testDirectoryToCopy";
            string subDirectory = "SubDirectory";

            string workDirectoryCopied = "testDirectoryCopied";

            string fileName1 = "TestText.txt";
            string fileName2 = "TestText2.txt";

            //Get Repo1 path 
            string directorieToCopy = Path.Combine(workingDirectory, workDirectory);
            string subDirectorieToCopy = Path.Combine(directorieToCopy, subDirectory);

            string file1InFirstDirectory = Path.Combine(directorieToCopy, fileName1);
            string file2InFirstDirectory = Path.Combine(directorieToCopy, fileName2);
            string file1InSubDirectory = Path.Combine(subDirectorieToCopy, fileName1);
            string file2InSubDirectory = Path.Combine(subDirectorieToCopy, fileName2);

            //Get Repo 2 Path
            string copiedDirectorie = Path.Combine(workingDirectory, workDirectoryCopied);


            //delete repo if already existing
            if (Directory.Exists(directorieToCopy))
                Directory.Delete(directorieToCopy,true);

            //Create repositories
            Directory.CreateDirectory(directorieToCopy);
            Directory.CreateDirectory(subDirectorieToCopy);

            using (var myFile = File.CreateText(file1InFirstDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file2InFirstDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file1InSubDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file2InSubDirectory)) myFile.Close();


            var myCreator = this;

            //Case_1, if everything is fine, nothing else should really happens. Nice to have,
            //get some logs to notify the change
            try
            {
                myCreator.CopyFolder(directorieToCopy, copiedDirectorie);

                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, fileName1))
                    , String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, fileName2))
                    , String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, subDirectory
                    , fileName1)), String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, subDirectory
                    , fileName2)), String.Format("Problem occured when try to copy the files..."));

            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Not able to copy the file, this excepton occured: \n{0}", e));
            }

            //Case_2, Should overwrite the current doc when restarted without generating any exception: 
            try
            {
                myCreator.CopyFolder(directorieToCopy, copiedDirectorie);

                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, fileName1))
                    , String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, fileName2))
                    , String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, subDirectory
                    , fileName1)), String.Format("Problem occured when try to copy the files..."));
                Assert.IsTrue(File.Exists(Path.Combine(copiedDirectorie, subDirectory, fileName2))
                    , String.Format("Problem occured when try to copy the files..."));
            }
            catch (Exception e)
            {
                Assert.Fail(String.Format("Not able to copy the file, this excepton occured: \n{0}", e));
            }

            //Case_3, try to copy a file wich is not existing:--> DirectoryNotFoundException 
            //The system should remark the error and report a File not found exception,
            //this exception should be then managed by the WorkArea builder object  

            if (Directory.Exists(directorieToCopy))
                Directory.Delete(directorieToCopy, true);

            Assert.ThrowsException<DirectoryNotFoundException>(
                () => { myCreator.CopyFolder(directorieToCopy, copiedDirectorie); }
                , String.Format("The expected exception didn't occured!"));



            //Case_4, Try to copy a file to a location wich is not reachable (Cause the target file is
            //blocked by an other process) --> IOException. Delete repo if already existing
            if (Directory.Exists(directorieToCopy))
                Directory.Delete(directorieToCopy, true);

            //Create repositories
            Directory.CreateDirectory(directorieToCopy);
            Directory.CreateDirectory(subDirectorieToCopy);

            using (var myFile = File.CreateText(file1InFirstDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file2InFirstDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file1InSubDirectory)) myFile.Close();
            using (var myFile = File.CreateText(file2InSubDirectory)) myFile.Close();
            myCreator.CopyFolder(directorieToCopy, copiedDirectorie);

            var openFile = File.Open(Path.Combine(copiedDirectorie, subDirectory, fileName2), FileMode.Open);


            Assert.ThrowsException<IOException>(
                () => { myCreator.CopyFolder(directorieToCopy, copiedDirectorie); }
                , String.Format("The expected exception didn't occured!"));

            openFile.Close();

            //Clean up:
            if (Directory.Exists(directorieToCopy))
                Directory.Delete(directorieToCopy, true);

            if (Directory.Exists(copiedDirectorie))
                Directory.Delete(copiedDirectorie, true);
            myCreator = null;
        }

        /// <summary>
        /// Case_1 try to access unreachable Folder on the SVN (Folder not created) 
        /// 
        /// --> SharpSvn.SvnRepositoryIOException --> Wrapped and commit as  IOException
        /// Case_2 Access is working properly
        /// 
        /// Case_3 some file can't be overwrited... --> SharpSvn.SvnSystemException
        /// --> Wrapped and commit as SystemException
        /// 
        /// CleanUp the test folder
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_CopyFilesFromSVN()
        {
            var myCreator = this;
            string badAdresseSource = 
                @"https://svn.rbi-online.de:8443/svn/Vendor/HelloError";
            string goodSource =
                @"https://svn.rbi-online.de:8443/svn/dtp/trunk/02_DbStruct/OneShot/ScriptSQL/TestCaseDB.db";
            
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string target = Path.Combine(workingDirectory, "SvnTest");
            

            //Case_1 try to access unreachable Folder on the SVN (Folder not created)
            Assert.ThrowsException<IOException>(() => { myCreator.CopyFilesFromSVN(badAdresseSource, target); });

            //Case_2 Access is working properly
            myCreator.CopyFilesFromSVN(goodSource, target);
            var fileList = Directory.GetFiles(target);
            Assert.IsTrue(fileList.Length > 0, "The repository seems to be empty after that the " +
                "SVN cmd was called !");

            //Case_3 some file can't be overwrited on local drive
            var stream = File.Open(fileList[0], FileMode.Open);
            Assert.ThrowsException<SystemException>(() => { myCreator.CopyFilesFromSVN(goodSource, target); });
            stream.Close();

            //CleanUp the test folder
            Directory.Delete(target, true);
        }

        /// <summary>
        /// Case 1 create a Directory with a file in open try to delete it and throw IOException
        /// Case 2 Normal use case
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_DeleteFolder()
        {
            //Case 1 create a Directory with a file in open try to delete it and throw IOException

            //Set up a txt file to copy in order to test
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());

            string workDirectory = "testDirectory";
            string fileName1 = "TestText.txt";

            //Get Repo1 path 
            string directorieToDelete = Path.Combine(workingDirectory, workDirectory);
            string file1InFirstDirectory = Path.Combine(directorieToDelete, fileName1);



            //delete repo if already existing
            if (Directory.Exists(directorieToDelete))
                Directory.Delete(directorieToDelete, true);

            //Create repositories and block it
            Directory.CreateDirectory(directorieToDelete);
            var myFile = File.CreateText(file1InFirstDirectory);

            var myCreator = this;
            Assert.ThrowsException<IOException>(
                () => 
                { myCreator.DeleteFolder(directorieToDelete); }
            );

            //Case 2 Normal use case
            myFile.Close();
            
            myCreator.DeleteFolder(directorieToDelete);
            Assert.IsTrue(!File.Exists(file1InFirstDirectory), "The file was not properly deleted!");



        }

        /// <summary>
        /// Case 1: normal case
        /// Case 2: Throw System.IO.IOException <-- Block Local folder to delete
        /// Case 3: LibGit2Sharp.LibGit2SharpException <-- Get wrong Path on Git
        /// Case 4: LibGit2Sharp.LibGit2SharpException <-- Get not possible Folder path 
        /// Case 5: LibGit2Sharp.LibGit2SharpException <-- Get wrong pass
        /// Case 6: LibGit2Sharp.LibGit2SharpException <-- Get wrong user
        /// Case 7: NullReferenceException <-- Get wrong branch, utils will need to restart to work properly
        /// </summary>
        [TestMethod]
        public void ComponentTesting_WACreator_CloneRepository()
        {
            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string gitPath;
            string localPath;
            string branch;
            string password;
            string username;

            string realPass = @"5pwmA2jwKnGQSNLAmu32";
            string realBranch = @"origin/master"; 
            string realUser = @"Samaritan57";
            string realLocalPath = Path.Combine(workingDirectory, "test");
            string realGitPath = @"https://Samaritan57@bitbucket.org/RBI-SWD/dtp_source.git";

            #region Case 1: normal case
            gitPath = realGitPath; 
            localPath = realLocalPath+"1";
            branch = realBranch; 
            password = realPass;
            username = realUser; 
            CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
            #endregion

            #region Case 2: Throw System.IO.IOException <-- Block Local folder to delete
            string blockerDemon = "SmallBlockerAHole.inYourA";
            using (File.CreateText(Path.Combine(localPath, blockerDemon)))
            {
                Assert.ThrowsException<System.IO.IOException>(
                () =>
                {
                    CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                }
                );
            }
            #endregion

            #region Case 3: LibGit2Sharp.LibGit2SharpException <-- Get wrong Path on Git
            gitPath = @"https://NiahahahaBadTime@ForYou.org/In/YourA.git";
            localPath = realLocalPath + "2";
            branch = realBranch;
            password = realPass;
            username = realUser; 
            Assert.ThrowsException<LibGit2Sharp.LibGit2SharpException>(
                 () =>
                 {
                     CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                 }
             );
            #endregion

            #region Case 4: LibGit2Sharp.LibGit2SharpException <-- Get not possible Folder path 
            gitPath = realGitPath;
            localPath = Path.Combine(@"O:\UnreachablePointInTheWorld", "test3");
            branch = realBranch;
            password = realPass;
            username = realUser;
            Assert.ThrowsException<LibGit2Sharp.LibGit2SharpException>(
                () =>
                {
                    CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                }
            );
            #endregion

            #region Case 5: LibGit2Sharp.LibGit2SharpException <-- Get wrong credential
            gitPath = realGitPath;
            localPath = realLocalPath + "5";
            branch = realBranch;
            password = "Abracada crash :p";
            username = "Bad user"; 
            Assert.ThrowsException<LibGit2Sharp.LibGit2SharpException>(
                 () =>
                 {
                     CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                 }
             );
            #endregion

            #region Case 6: LibGit2Sharp.LibGit2SharpException <-- Get wrong user
            gitPath = realGitPath;
            localPath = realLocalPath + "5";
            branch = realBranch;
            password = realPass;
            username = @"WhatSMyName"; 
            Assert.ThrowsException<LibGit2Sharp.LibGit2SharpException>(
                 () =>
                 {
                     CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                 }
             );
            #endregion

            #region Case 6: NullReferenceException <-- Get wrong branch, utils will need to restart to work properly
            gitPath = realGitPath;
            localPath = realLocalPath + "5";
            branch = @"origin/feature/WhereYouGo";
            password = realPass;
            username = realUser;
            Assert.ThrowsException<NullReferenceException>(
                 () =>
                 {
                     CloneProjectAndSetUpBranche(localPath, gitPath, branch, password, username);
                 }
             );
            #endregion
        }
    } 
}