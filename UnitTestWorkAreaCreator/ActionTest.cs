﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic; 
using WorkAreaCreatorLib;
using WorkAreaCreatorLib.Configs;

using System.Security.Principal;
using System.Security.AccessControl;

namespace UnitTestWorkAreaCreator
{

    /// <summary>
    /// Test Action per action
    ///     - Check object with wrong paramter
    ///     - Serialize 
    ///     - De-Serialize
    ///     - Call MockUserRequest to fix issue (Mock need to be set up with correct value)
    ///     - Call acion Execute --> Will trigger the user request to the mock
    ///     - Serialize 
    ///     - De-Serialize
    ///     - Execute 
    ///     --> Done if execute return true. 
    /// </summary>
    [TestClass]
    public class Engine_ImplementedActionTest : IEngineGuiInteraction
    {
        #region global Var and Mtd (used in nearly all action)
        //PAth where file are stored during serialization (Use for unitTestPurpose)
        string Path_TempBuildRepo = Path.GetFullPath(@"..\tempXmlBuildObject");

        private void initFileFolderTempRepo()
        {
            Directory.CreateDirectory(Path_TempBuildRepo);
            Path_FolderToCopy = Path.Combine(Path_TempBuildRepo, "FolderCopyTest");
            fileListForFolderCopy = new List<string>();
            for (int i = 1; i < 6; i++)
            {
                var path = Path.Combine(Path_FolderToCopy, $"SubRepo_{i}");
                Directory.CreateDirectory(path);

                for (int j = 1; j < 6; j++)
                {
                    var filePath = Path.Combine(path, $"TxtFile_{j}.txt");
                    fileListForFolderCopy.Add(filePath);
                    using (var myStreamWritter = File.CreateText(filePath))
                        myStreamWritter.WriteLine("HelloWorld" + $":{filePath}: hello hello");
                }
            }
        }
        
        private void SetRightsAndDelete(string path)
        {
            foreach (string f in Directory.GetFiles(path))
            {
                //Add to currentDirectory Write access
                DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(f));
                DirectorySecurity dSecurity = dirInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.InheritOnly, AccessControlType.Allow));
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        WindowsIdentity.GetCurrent().Name,
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.InheritOnly, AccessControlType.Allow));
                // Set the access control
                dirInfo.SetAccessControl(dSecurity);

                //Remove the readOnly flag
                var fileInfo = new FileInfo(f);
                fileInfo.IsReadOnly = false; 
                fileInfo.Attributes = FileAttributes.Normal;

                fileInfo.Delete(); 

                //Add full file access for current session
                //FileSecurity fSecurity = File.GetAccessControl(f);
                //var id = WindowsIdentity.GetCurrent();
                //fSecurity.AddAccessRule(new FileSystemAccessRule(id.Name, FileSystemRights.FullControl, AccessControlType.Allow ));
                //File.SetAccessControl(f, fSecurity);

            }
            foreach (string d in Directory.GetDirectories(path))
                SetRightsAndDelete(d);
        }
        private void cleanTempRepo()
        {
            try
            {
                Directory.Delete(Path_TempBuildRepo, true);
            }catch
            {
                //Add to currentDirectory Write access
                DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(Path_TempBuildRepo));
                DirectorySecurity dSecurity = dirInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        WindowsIdentity.GetCurrent().Name,
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                dirInfo.SetAccessControl(dSecurity); 


                SetRightsAndDelete(Path_TempBuildRepo);
                Directory.Delete(Path_TempBuildRepo, true);
            }
        }
        private void SerializeDeserializeAndInit<T>(ref T toSerialize, string atPath) where T : ExecutableAction
        {
            Console.WriteLine($"Serialize  and deserialize {toSerialize.GetType()} object.");
            BaseSerializerDeserializer xmlSerializerUtils = new BaseSerializerDeserializer();
            Assert.IsTrue(xmlSerializerUtils.SerializeObjetToXml
                (toSerialize, atPath));
            Assert.IsTrue(xmlSerializerUtils.DeserializeXmlToObject
                (out toSerialize, atPath));
            toSerialize.Initialized(this);
            Console.WriteLine(" --> Pass");
        }
        #endregion

        #region Var for Action_ObjFctTesting_CopyFileToLocation & Action_ObjFctTesting_CopyFolerToLocation
        string Path_FolderToCopy;
        List<string> fileListForFolderCopy;
        #endregion
        
        #region IEngineGuiInteraction implementation mockingPart
        public void Initialize(WACreatorController creator)
        {
            Console.WriteLine("Tedah, Pidou, just Initialize the stuff.");
        }

        public void Notify(LogLevel level, string text)
        {
            Console.WriteLine($"ActionLogs: {level}: {text}");
        }

        private object classLocker = new object();

        private string _PathModifRequestResult;
        string PathModifRequestResult
        {
            get { lock (classLocker) return _PathModifRequestResult; }
            set { lock (classLocker) _PathModifRequestResult = value; }
        }

        private bool _ChangeRequestUnlock;
        private bool ChangeRequestUnlock
        {
            get { lock (classLocker) return _ChangeRequestUnlock; }
            set { lock (classLocker) _ChangeRequestUnlock = value; }
        }
        private bool _ChangeRequestNewPath;
        private bool ChangeRequestNewPath
        {
            get { lock (classLocker) return _ChangeRequestNewPath; }
            set { lock (classLocker) _ChangeRequestNewPath = value; }
        }

        //if msg = "beTrue" with current value of ToSend path (Thread safe access);
        public bool RequestUserToChangePath(string msg, ref string path)
        {
            if (ChangeRequestNewPath)
                path = PathModifRequestResult;

            return ChangeRequestNewPath;
        }

        FileStream fs = null;
        private void BlockFile(string filePath)
        {
            lock (classLocker)
            {
                try {
                    if (fs != null)
                        fs.Dispose();
                }
                catch{ }

                if (!File.Exists(filePath))
                    //create file to copy
                    using (var myStreamWritter = File.CreateText(filePath))
                    {
                        myStreamWritter.WriteLine("HelloWorld");
                    }
                fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None);
            }
        }
        private void UnlockFile()
        {
            lock (classLocker)
                if (fs != null)
                    fs.Dispose();
        }

        //Return true if message = "beTrue"
        public bool RequestUserToUnlockFile(string message)
        {
            if (ChangeRequestUnlock)
                UnlockFile();
            return ChangeRequestUnlock;
        }
        #endregion

        /// <summary>
        /// There is no recovery possibility on this one.
        ///  --> Check Serialization and Deserialization
        /// </summary>
        [TestMethod]
        public void Action_ObjectFctTesting_BuildEtFwProject()
        {
            initFileFolderTempRepo();
            #region var definition 
            string Path_BuildProjectXmlFile = Path.Combine(Path_TempBuildRepo, @"BuildEtFw.xml");
            #endregion

            string projectFileName = @"C:\WorkAreas\WorkAreaCreatorV1\UnitTestWorkAreaCreator\bin\Debug\" +
                @"SdsTestJob_ECU_Ntg7\EtFwUtils\SpeechService\SpeechSynthesizer.sln";
            string configuration = "Debug";
            string platform = "Any CPU";

            Console.WriteLine("Step: Create Object");
            BuildProject toTestBuild = new BuildProject();
            toTestBuild.ProjectPath.Value = projectFileName;
            toTestBuild.Configuration.Value = configuration;
            toTestBuild.Platform.Value = platform;
            Console.WriteLine(" --> Pass");

            Console.WriteLine("Serialize and deserialize the object");
            SerializeDeserializeAndInit(ref toTestBuild, Path_BuildProjectXmlFile);

            Console.WriteLine("   Step: Execute build and verify it's working.");
            toTestBuild.Initialized(this);
            Assert.IsTrue(toTestBuild.Execute());
            Console.WriteLine(" --> Pass");

            cleanTempRepo();

        }


        private void Build(
            string projectFileName, string configuration, string platform)
        {

            initFileFolderTempRepo();
            string Path_BuildProjectXmlFile = Path.Combine(Path_TempBuildRepo, @"TempBuild.xml");

            Console.WriteLine("Step: Create Object");
            BuildProject toTestBuild = new BuildProject();
            toTestBuild.ProjectPath.Value = projectFileName;
            toTestBuild.Configuration.Value = configuration;
            toTestBuild.Platform.Value = platform;
            Console.WriteLine(" --> Pass");

            Console.WriteLine("Serialize and deserialize the object");
            SerializeDeserializeAndInit(ref toTestBuild, Path_BuildProjectXmlFile);

            Console.WriteLine("   Step: Execute build and verify it's working.");
            toTestBuild.Initialized(this);
            Assert.IsTrue(toTestBuild.Execute());
            Console.WriteLine(" --> Pass");

            cleanTempRepo();

        }
        [TestMethod]
        public void Action_ObjectFctTesting_BuildDltV1t()
        {
            Build(@"C:\WorkAreas\WorkAreaCreatorV1\UnitTestWorkAreaCreator\bin\Debug\"
                + @"SdsTestJob_ECU_Ntg7\DltViewerV1\DltViewer\DltViewer.sln",
                "NTG7", 
                "Any CPU"); 
        }
        [TestMethod]
        public void Action_ObjectFctTesting_BuildEtFwUtils()
        {
            Build(@"C:\WorkAreas\WorkAreaCreatorV1\UnitTestWorkAreaCreator\bin\Debug\"
                + @"SdsTestJob_ECU_Ntg7\DltViewerV1\DltViewer\DltViewer.sln",
                "NTG7",
                "Any CPU");
        }


        [TestMethod]
        public void Action_ObjectFctTesting_BuildProject()
        {

            initFileFolderTempRepo();

            #region var definition 
            string Path_BuildProjectXmlFile = Path.Combine(Path_TempBuildRepo, @"buildProject.xml");
            #endregion


            #region case 1: Not working case

            Console.WriteLine("1] Case 1, not working case");

            string projectFileName = @"..\..\UnitTestExternNeeds\NonExestingSln.sln";
            string configuration = "Debug";
            string platform = "Any CPU";

            Console.WriteLine("   Step: Create Object");
            BuildProject toTestBuild = new BuildProject();
            toTestBuild.ProjectPath.Value = projectFileName;
            toTestBuild.Configuration.Value = configuration;
            toTestBuild.Platform.Value = platform;

            Console.WriteLine(" --> Pass");

            SerializeDeserializeAndInit(ref toTestBuild, Path_BuildProjectXmlFile);

            Console.WriteLine("   Step: Execute build and verify it's failling....");
            toTestBuild.Initialized(this);
            Assert.IsFalse(toTestBuild.Execute());
            Console.WriteLine(" --> Pass");
            #endregion

            #region case 2: Working case
            projectFileName = @"..\..\UnitTestExternNeeds\DummyBuild\DummyBuild.sln";
            configuration = "Debug";
            platform = "Any CPU";


            Console.WriteLine("   Step: Create Object");
            toTestBuild = new BuildProject();
            toTestBuild.ProjectPath.Value = projectFileName;
            toTestBuild.Configuration.Value = configuration;
            toTestBuild.Platform.Value = platform;

            Console.WriteLine(" --> Pass");

            SerializeDeserializeAndInit(ref toTestBuild, Path_BuildProjectXmlFile);

            Console.WriteLine("   Step: Execute build and verify it's working.");
            toTestBuild.Initialized(this);
            Assert.IsTrue(toTestBuild.Execute());
            Console.WriteLine(" --> Pass");
            #endregion

            cleanTempRepo();

        }

        [TestMethod]
        public void Action_ObjFctTesting_CopyFileToLocation()
        {
            initFileFolderTempRepo();

            #region var definition
            string Path_FileSource = Path.Combine(Path_TempBuildRepo, "toCopy.txt");
            string Path_FileTarget = Path.Combine(Path_TempBuildRepo, "toCopy2.txt");
            string Path_CopyFileToLocationXmlFile = Path.Combine(Path_TempBuildRepo, @"CopyFileToLocation.xml");
            CopyFileToLocation copyFileToLocation;
            #endregion

            //create file_source to copy
            using (var myStreamWritter = File.CreateText(Path_FileSource))
            {
                myStreamWritter.WriteLine("HelloWorld");
            }

            //Create base object with wrong source name.
            copyFileToLocation = new CopyFileToLocation();
            copyFileToLocation.SourceFile.Value = @"..\nonExistingpath";
            copyFileToLocation.TargetPath.Value = Path_FileTarget;
            


            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            #region case 1: Wrong source path:
            // basically, the ChangeRequestNew path will return false and obj.execute() shall return false either.
            // If the request is ok, it should return true ...
            // But the object path shall be corrected with the correct current path.
            // Check the assert values at the end of the region to check how it's working

            Console.WriteLine("1] Test with wrong SourceFilePath"); 
            Console.WriteLine("   1-a] User cancel action");
            lock (classLocker)
            {
                if (File.Exists(Path_FileTarget)) File.Delete(Path_FileTarget);
                PathModifRequestResult = null; 
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }
            Assert.IsFalse(copyFileToLocation.Execute());
            Assert.AreEqual(copyFileToLocation.SourceFile.Value, @"..\nonExistingpath");
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            Console.WriteLine("   1-b] User give a valid Path");
            lock (classLocker)
            {
                PathModifRequestResult = Path_FileSource;
                ChangeRequestNewPath = true;
                copyFileToLocation.SourceFile.Value = @"..\nonExistingpath"; 
                if (File.Exists(Path_FileTarget)) File.Delete(Path_FileTarget);
            }
            Assert.IsTrue(copyFileToLocation.Execute());
            Assert.AreEqual(copyFileToLocation.SourceFile.Value, Path_FileSource);
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            #endregion

            #region Case 2: Blocked targeted path

            Console.WriteLine("2] Test with a write-blocked file,");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                if (File.Exists(Path_FileTarget)) File.Delete(Path_FileTarget);
            }

            Console.WriteLine("    2-a] User cancel action change and unlock action");
            BlockFile(Path_FileTarget);
            Assert.IsFalse(copyFileToLocation.Execute());
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            lock (classLocker)
            {
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }

            Console.WriteLine("    2-b] User unlock the file");
            Assert.IsTrue(copyFileToLocation.Execute());
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            #endregion

            #region case 3: Working case
            Console.WriteLine("3] User give good parameters:");
            
            copyFileToLocation = new CopyFileToLocation();
            copyFileToLocation.SourceFile.Value = Path_FileSource;
            copyFileToLocation.TargetPath.Value = Path_FileTarget;

            copyFileToLocation.Initialized(this);

            Assert.IsTrue(copyFileToLocation.Execute());
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);
            #endregion

            #region case 4: User give a wrong outputPath, script need to be review...
            Console.WriteLine("4] User give a wrong target path, and can't unlock or change path");

            Console.WriteLine("4-a] User can't unlock or don't want to change path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
            }
            
            copyFileToLocation = new CopyFileToLocation();
            copyFileToLocation.SourceFile.Value = Path_FileSource;
            copyFileToLocation.TargetPath.Value = @"E:\UknowTarget";

            copyFileToLocation.Initialized(this);
            Assert.IsFalse(copyFileToLocation.Execute());
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);


            Console.WriteLine("4-b] User is changing path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
                PathModifRequestResult = Path_FileTarget;
            }
            copyFileToLocation.Initialized(this);
            Assert.IsTrue(copyFileToLocation.Execute());
            SerializeDeserializeAndInit(ref copyFileToLocation, Path_CopyFileToLocationXmlFile);

            #endregion

            cleanTempRepo();
        }

        [TestMethod]
        public void Action_ObjFctTesting_CopyFolerToLocation()
        {
            initFileFolderTempRepo();

            #region var definition

            string Path_CopyFolderToFolder = Path.Combine(Path_FolderToCopy, @"..\CopiedFolder");
            string Path_CopyFileToLocationXmlFile = Path.Combine(Path_TempBuildRepo, @"CopyFileToLocation.xml");
            CopyFolderToLocation copyFolderAction;
            #endregion

            #region case1: Wrong input folder 
            copyFolderAction = new CopyFolderToLocation();
            copyFolderAction.SourceFolder.Value = @"..\nonExistingpath";
            copyFolderAction.TargetPath.Value = Path_CopyFolderToFolder;
            
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);

            Console.WriteLine("1] Test with non existent SourceFolderPath");
            lock (classLocker)
            {
                if (Directory.Exists(Path_CopyFolderToFolder))
                    Directory.Delete(Path_CopyFolderToFolder, true);
                PathModifRequestResult = null;
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }
            Console.WriteLine("   1-a] User cancel action");
            Assert.IsFalse(copyFolderAction.Execute());
            Assert.AreEqual(copyFolderAction.SourceFolder.Value, @"..\nonExistingpath");
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);

            Console.WriteLine("   1-b] User give a valid Path");
            lock (classLocker)
            {
                PathModifRequestResult = Path_FolderToCopy;
                ChangeRequestNewPath = true;
                copyFolderAction.SourceFolder.Value = @"..\nonExistingpath";
                if (Directory.Exists(Path_CopyFolderToFolder))
                    Directory.Delete(Path_CopyFolderToFolder, true);
            }
            Assert.IsTrue(copyFolderAction.Execute());
            Assert.AreEqual(copyFolderAction.SourceFolder.Value, Path_FolderToCopy);
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);
            #endregion


            #region Case 2: Blocked targeted path

            Console.WriteLine("2] Test with a write-blocked file,");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                if (Directory.Exists(Path_CopyFolderToFolder))
                    Directory.Delete(Path_CopyFolderToFolder, true);
            }

            Console.WriteLine("    2-a] User cancel action change and unlock action");

            foreach (var fileToBlock in fileListForFolderCopy)
            {
                BlockFile(fileToBlock);
                Assert.IsFalse(copyFolderAction.Execute());
                SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);
            }


            Console.WriteLine("    2-b] User unlock the file");
            lock (classLocker)
            {
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }

            foreach (var fileToBlock in fileListForFolderCopy)
            {
                BlockFile(fileToBlock);
                Assert.IsTrue(copyFolderAction.Execute());
                SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);
            }

            #endregion


            #region case 3: Working case
            Console.WriteLine("3] User give good parameters:");
            copyFolderAction = new CopyFolderToLocation();
            copyFolderAction.SourceFolder.Value = Path_FolderToCopy;
            copyFolderAction.TargetPath.Value = Path_CopyFolderToFolder;
            
            copyFolderAction.Initialized(this);
            Assert.IsTrue(copyFolderAction.Execute());
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);
            #endregion

            #region case 4: User give a wrong outputPath, script need to be review...
            Console.WriteLine("4] User give a wrong folder target path, and can't unlock or change path");

            Console.WriteLine("4-a] User can't unlock or don't want to change path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
            }
            copyFolderAction = new CopyFolderToLocation();
            copyFolderAction.SourceFolder.Value = Path_FolderToCopy;
            copyFolderAction.TargetPath.Value = @"E:\UknowTarget";
            copyFolderAction.Initialized(this);
            Assert.IsFalse(copyFolderAction.Execute());
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);

            Console.WriteLine("4-b] User is changing path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
                PathModifRequestResult = Path_CopyFolderToFolder;
            }
            copyFolderAction.Initialized(this);
            Assert.IsTrue(copyFolderAction.Execute());
            SerializeDeserializeAndInit(ref copyFolderAction, Path_CopyFileToLocationXmlFile);

            #endregion

            cleanTempRepo();

        }

        [TestMethod]
        public void Action_ObjFctTesting_GitExport()
        {
            string localRepo = Path.Combine(Path_TempBuildRepo, "gitExport");
            string configRepo = Path.Combine(Path_TempBuildRepo, @"config/GitExport.xml");
            initFileFolderTempRepo();

            #region case1: Working case:
            var actToTest = new GitExport();
            actToTest.UserName.Value = "Samaritan57";
            actToTest.Password.Value = "5pwmA2jwKnGQSNLAmu32";
            actToTest.Branch.Value = "master";
            actToTest.OnlineRepository.Value = "https://Samaritan57@bitbucket.org/Samaritan57/global_feature.git";
            actToTest.LocalRepository.Value = localRepo;
            
            actToTest.Initialized(this);

            SerializeDeserializeAndInit(ref actToTest, configRepo);
            actToTest.Execute();


            #endregion


            cleanTempRepo(); 

        }

        [TestMethod]
        public void Action_ObjFctTesting_LocationDeletion()
        {
            string configRepo = Path.Combine(Path_TempBuildRepo, @"config/LocationDeletion.xml");
            Console.WriteLine("1] Normal working condition");
            initFileFolderTempRepo();
            Console.WriteLine("  1.a] Location is found, Ahould delete without problem.");
            var actionToTest = new LocationDeletion();
            actionToTest.TargetToDelete.Value = Path_TempBuildRepo; 
            SerializeDeserializeAndInit(ref actionToTest, configRepo);

            Assert.IsTrue( actionToTest.Execute());
            Assert.IsTrue(!Directory.Exists(Path_TempBuildRepo));

            Console.WriteLine("  1.b] Location not found, no need to delete should return 'OK'");
            Assert.IsTrue(actionToTest.Execute());
            Assert.IsTrue(!Directory.Exists(Path_TempBuildRepo));


            Console.WriteLine("2] Blocked file. ");
            initFileFolderTempRepo();

            string Path_FileSource = Path.Combine(Path_TempBuildRepo, "toBlock.txt");
            using (var myStreamWritter = File.CreateText(Path_FileSource))
            {
                myStreamWritter.WriteLine("HelloWorld");
            }
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
            }
            BlockFile(Path_FileSource); 

            Console.WriteLine("   2.a] User is not unlocking file.");
            Assert.IsFalse(actionToTest.Execute());
            
            Console.WriteLine("   2.b] User is unlocking file.");

            lock (classLocker)
            {
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }
            Assert.IsTrue(actionToTest.Execute());
        }

        [TestMethod]
        public void Action_ObjectFctTesting_SvnExport()
        {
            string badAdresseSource =
                @"https://svn.rbi-online.de:8443/svn/Vendor/HelloError";
            string goodSource =
                @"https://svn.rbi-online.de:8443/svn/dtp/trunk/02_DbStruct/OneShot/ScriptSQL/TestCaseDB.db";

            string goodTarget = @"svnTest"; 
            string badTarget = @"S:\UnknowPlace";

            string serializedObjFile = Path.Combine(Path_TempBuildRepo, @"config/SvnExport.xml");

            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string target = Path.Combine(Path_TempBuildRepo, goodTarget);

            var toTest = new SvnExport();
            toTest.SvnRemotePath.Value = badAdresseSource; 
            toTest.LocalDrivePath.Value = target;

            SerializeDeserializeAndInit(ref toTest, serializedObjFile); 


            Console.WriteLine("1] try to access unreachable Folder on the SVN (Folder not created)");
            Console.WriteLine("   1.a] User refuse to change Path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                PathModifRequestResult = null; 
            }
            Assert.IsFalse(toTest.Execute());

            Console.WriteLine("   1.b] User give new correct Path.");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
                PathModifRequestResult = goodSource;
            }
            Assert.IsTrue(toTest.Execute());

            Console.WriteLine("2] Access is working properly"); 
            SerializeDeserializeAndInit(ref toTest, serializedObjFile);
            toTest.Execute();

            Console.WriteLine("3] Some file can't be overwrited on local drive, lock the system.");
            Console.WriteLine("3.a] User refuse to unlock");
            var fileList = Directory.GetFiles(target);
            BlockFile(fileList[0]);
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
            }
            Assert.IsFalse(toTest.Execute()); 

            Console.WriteLine("3.b] User is unlocking the repo");

            lock (classLocker)
            {
                ChangeRequestUnlock = true;
                ChangeRequestNewPath = false;
            }
            Assert.IsTrue(toTest.Execute());
            Console.WriteLine("4] Impossible Local Folder gived as parameter.");
            toTest = new SvnExport();
            toTest.SvnRemotePath.Value = goodSource;  
            toTest.LocalDrivePath.Value = badTarget;

            SerializeDeserializeAndInit(ref toTest, serializedObjFile);
            Console.WriteLine("4.a] User is not providing new path."); 
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
            }
            Assert.IsFalse(toTest.Execute());
            Console.WriteLine("4.b] User providing new path.");
            lock (classLocker)
            {
                PathModifRequestResult = goodTarget; 
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
            }
            Assert.IsTrue(toTest.Execute());

        }

        [TestMethod]
        public void Action_UnzipTo()
        {


            string goodInputZipFile = "ToUnzipFolder.zip";
            string notExistingIZF = "ImNotExisting.zip";

            string goodTarget = @"ZipTestRepo";
            string badTarget = @"S:\UnknowPlace";

            string serializedObjFile = Path.Combine(Path_TempBuildRepo, @"config/UnzipTo.xml");

            string workingDirectory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            string target = Path.Combine(Path_TempBuildRepo, goodTarget);



            var toTest = new UnZipTo();
            toTest.InputZip.Value = notExistingIZF;
            toTest.OutputRepo.Value = goodTarget;

            SerializeDeserializeAndInit(ref toTest, serializedObjFile);


            Console.WriteLine("1] try to access unreachable zip on the PC");
            Console.WriteLine("   1.a] User refuse to change Path");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                PathModifRequestResult = null;
            }

            //CleanUpTherepos
            if (Directory.Exists(goodTarget))
                Directory.Delete(goodTarget, true);
            Assert.IsFalse(toTest.Execute());

            Console.WriteLine("   1.b] User give new correct Path.");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
                PathModifRequestResult = goodInputZipFile;
            }

            //CleanUpTherepos
            if (Directory.Exists(goodTarget))
                Directory.Delete(goodTarget, true);
            Assert.IsTrue(toTest.Execute());


            Console.WriteLine("2] Try to write on unreachable disk on the PC");
            Console.WriteLine("   2.a] User refuse to change Path");

            toTest.InputZip.Value = goodInputZipFile;
            toTest.OutputRepo.Value = badTarget;

            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                PathModifRequestResult = null;
            }

            //CleanUpTherepos
            if (Directory.Exists(goodTarget))
                Directory.Delete(goodTarget, true);
            Assert.IsFalse(toTest.Execute());

            Console.WriteLine("   1.b] User give new correct Path.");
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = true;
                PathModifRequestResult = goodTarget;
            }

            //CleanUpTherepos
            if (Directory.Exists(goodTarget))
                Directory.Delete(goodTarget, true);
            Assert.IsTrue(toTest.Execute());
        }
    }

}
