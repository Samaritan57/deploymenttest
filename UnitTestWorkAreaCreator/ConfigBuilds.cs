﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Principal;
using System.Security.AccessControl;

using WorkAreaCreatorLib.Configs;
using WorkAreaCreatorLib;

namespace UnitTestWorkAreaCreator
{
    [TestClass]
    public class ConfigBuilds: IEngineGuiInteraction
    {
    #region IEngineGuiInteraction
        #region global Var and Mtd (used in nearly all action)
        //PAth where file are stored during serialization (Use for unitTestPurpose)
        string Path_TempBuildRepo = Path.GetFullPath(@"..\tempXmlBuildObject");
        private void initFileFolderTempRepo()
        {
            Directory.CreateDirectory(Path_TempBuildRepo);
            Path_FolderToCopy = Path.Combine(Path_TempBuildRepo, "FolderCopyTest");
            fileListForFolderCopy = new List<string>();
            for (int i = 1; i < 6; i++)
            {
                var path = Path.Combine(Path_FolderToCopy, $"SubRepo_{i}");
                Directory.CreateDirectory(path);

                for (int j = 1; j < 6; j++)
                {
                    var filePath = Path.Combine(path, $"TxtFile_{j}.txt");
                    fileListForFolderCopy.Add(filePath);
                    using (var myStreamWritter = File.CreateText(filePath))
                        myStreamWritter.WriteLine("HelloWorld" + $":{filePath}: hello hello");
                }
            }
        }
        private void SetRightsAndDelete(string path)
        {
            foreach (string f in Directory.GetFiles(path))
            {
                //Add to currentDirectory Write access
                DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(f));
                DirectorySecurity dSecurity = dirInfo.GetAccessControl();

                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.InheritOnly, AccessControlType.Allow));
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        WindowsIdentity.GetCurrent().Name,
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.InheritOnly, AccessControlType.Allow));

                // Set the access control
                dirInfo.SetAccessControl(dSecurity);

                //Remove the readOnly flag
                var fileInfo = new FileInfo(f);
                fileInfo.IsReadOnly = false;
                fileInfo.Attributes = FileAttributes.Normal;

                fileInfo.Delete();

            }
            foreach (string d in Directory.GetDirectories(path))
                SetRightsAndDelete(d);
        }
        private void cleanTempRepo()
        {
            try
            {
                Directory.Delete(Path_TempBuildRepo, true);
            }
            catch
            {
                //Add to currentDirectory Write access
                DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(Path_TempBuildRepo));
                DirectorySecurity dSecurity = dirInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                dSecurity.AddAccessRule(
                    new FileSystemAccessRule(
                        WindowsIdentity.GetCurrent().Name,
                        FileSystemRights.FullControl,
                        InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                        PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
                dirInfo.SetAccessControl(dSecurity);


                SetRightsAndDelete(Path_TempBuildRepo);
                Directory.Delete(Path_TempBuildRepo, true);
            }
        }
        private void SerializeDeserializeAndInit<T>(ref T toSerialize, string atPath) where T : ExecutableAction
        {
            Console.WriteLine($"Serialize  and deserialize {toSerialize.GetType()} object.");
            BaseSerializerDeserializer xmlSerializerUtils = new BaseSerializerDeserializer();
            Assert.IsTrue(xmlSerializerUtils.SerializeObjetToXml
                (toSerialize, atPath));
            Assert.IsTrue(xmlSerializerUtils.DeserializeXmlToObject
                (out toSerialize, atPath));
            toSerialize.Initialized(this);
            Console.WriteLine(" --> Pass");
        }
        #endregion

        #region Var for Action_ObjFctTesting_CopyFileToLocation & Action_ObjFctTesting_CopyFolerToLocation
        string Path_FolderToCopy;
        List<string> fileListForFolderCopy;
        #endregion

        #region IEngineGuiInteraction implementation mockingPart
        public void Initialize(WACreatorController creator)
        {
            Console.WriteLine("Tedah, Pidou, just Initialize the stuff.");
        }

        public void Notify(LogLevel level, string text)
        {
            Console.WriteLine($"ActionLogs: {level}: {text}");
        }

        private object classLocker = new object();

        private string _PathModifRequestResult;
        string PathModifRequestResult
        {
            get { lock (classLocker) return _PathModifRequestResult; }
            set { lock (classLocker) _PathModifRequestResult = value; }
        }

        private bool _ChangeRequestUnlock;
        private bool ChangeRequestUnlock
        {
            get { lock (classLocker) return _ChangeRequestUnlock; }
            set { lock (classLocker) _ChangeRequestUnlock = value; }
        }
        private bool _ChangeRequestNewPath;
        private bool ChangeRequestNewPath
        {
            get { lock (classLocker) return _ChangeRequestNewPath; }
            set { lock (classLocker) _ChangeRequestNewPath = value; }
        }

        //if msg = "beTrue" with current value of ToSend path (Thread safe access);
        public bool RequestUserToChangePath(string msg, ref string path)
        {
            if (ChangeRequestNewPath)
                path = PathModifRequestResult;

            return ChangeRequestNewPath;
        }

        FileStream fs = null;
        private void BlockFile(string filePath)
        {
            lock (classLocker)
            {
                try
                {
                    if (fs != null)
                        fs.Dispose();
                }
                catch { }

                if (!File.Exists(filePath))
                    //create file to copy
                    using (var myStreamWritter = File.CreateText(filePath))
                    {
                        myStreamWritter.WriteLine("HelloWorld");
                    }
                fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None);
            }
        }
        private void UnlockFile()
        {
            lock (classLocker)
                if (fs != null)
                    fs.Dispose();
        }

        //Return true if message = "beTrue"
        public bool RequestUserToUnlockFile(string message)
        {
            if (ChangeRequestUnlock)
                UnlockFile();
            return ChangeRequestUnlock;
        }
        #endregion
    #endregion

        private void OnError(object caller, string message)
        {
            Console.WriteLine($"{caller}:{message}");
        }

        [TestMethod]
        public void GenerateAndLoad_NTG7_Config()
        {
            //Do not accept to change anything. (should work at first attempt)
            lock (classLocker)
            {
                ChangeRequestUnlock = false;
                ChangeRequestNewPath = false;
                PathModifRequestResult = null;
            }
            var EnvToBuild = GenerateNtg7ConfigObject();
            EnvToBuild.Error += OnError; 
            Assert.IsTrue(EnvToBuild.Serialize(@"../../../WorkAreaCreatorLib/Configurator/OneShotNtg7.xml"));

            var deserializedENV = new EnvironementBuildInstructions().UnserializedFromPath(@"../../../WorkAreaCreatorLib/Configurator/OneShotNtg7.xml"); 

            Assert.IsTrue(deserializedENV.BuildEnvironment(this)); 
            EnvToBuild.Error -= OnError; 
        }


        private ParametersDef AddToGlobalListAndGetRef(string ID, string Value, ParameterValueType type
            , ref EnvironementBuildInstructions config)
        {
            ParametersDef toReturn = new ParametersDef(
                Value,
                type);
            config.GlobalParameters.Add(ID, toReturn);
            return toReturn; 
        }
        private EnvironementBuildInstructions GenerateNtg7ConfigObject()
        {
            EnvironementBuildInstructions toReturn = new EnvironementBuildInstructions();

            #region Global parameters

            string etFwVersion = "v10.8.3"; 
            string thriftVersion = "E434(2.23.00)_10.5.2.f3f7b6c";
            string workAreaName = "SdsTestJob_ECU_Ntg7";
            string etFwBinFolder = "02_Runtime";

            #region EtFw params
            string idFolderEtFwLibPath = "EtFwFolderPath";
            ParametersDef FolderEtFwLibPath = AddToGlobalListAndGetRef( idFolderEtFwLibPath,
                @"\\rbistorage03\Auster\01_EnvironmentLib\00_EtFw\Pre-Releases",
                ParameterValueType.FixedValue, ref toReturn);

            string idEtFwZipVersion = "ZipEtFwVersion"; 
            ParametersDef EtFwZipVersion = AddToGlobalListAndGetRef(idEtFwZipVersion, 
                etFwVersion+".zip", ParameterValueType.FixedValue, ref toReturn);

            string idEtFwVersion = "EtFwVersion";
            ParametersDef EtFwVersion = AddToGlobalListAndGetRef(idEtFwVersion,
                etFwVersion, ParameterValueType.FixedValue, ref toReturn);

            string idEtFwBinFolder = "EtFwBinFolder";
            ParametersDef EtFwBinFolder = AddToGlobalListAndGetRef(idEtFwBinFolder, 
                etFwBinFolder, ParameterValueType.FixedValue, ref toReturn);

            #endregion

            #region thrift params
            string idFolderThriftLibPath = "ThriftFolderPath";
            ParametersDef FolderThriftLibPath = AddToGlobalListAndGetRef( idFolderThriftLibPath,
                @"\\rbistorage03\Auster\01_EnvironmentLib\01_Thrift\Thrift-Releases\NTG7",
                ParameterValueType.FixedValue, ref toReturn);

            string idThriftZipVersion = "ThriftZipVersion";
            ParametersDef ThriftZipVersion = AddToGlobalListAndGetRef( idThriftZipVersion , 
                thriftVersion + ".zip", ParameterValueType.FixedValue, ref toReturn);

            string idThriftVersion = "ThriftVersion";
            ParametersDef ThriftVersion = AddToGlobalListAndGetRef( idThriftVersion, 
                thriftVersion, ParameterValueType.FixedValue, ref toReturn);

            string idThriftLocation = "ThriftLocation"; 
            ParametersDef ThriftLocation = AddToGlobalListAndGetRef( idThriftLocation,
                $"{idFolderThriftLibPath};{idThriftZipVersion}",
                ParameterValueType.PathCombination, ref toReturn);

            #endregion

            #region GitRepos
            string idOneShotGitUrl = "GitUrlSttOneShot";
            string gitUrlForOneShotGit = @"https://Samaritan57@bitbucket.org/Samaritan57/sttprojectsource.git";
            string idOneShotGitBranch = "GitBranchOneShot";
            string gitBranchSttOneShot = "master";
            string idOneShotLocalRepo = "SttTestJobRepoName";
            string valOneShotLocalRepo = "SttTestJob";

            string idEtFwUtilsGitUrl = "GitUrlEtFwUtils";
            string gitUrlEtFwUtils = @"https://Samaritan57@bitbucket.org/Samaritan57/etfw_utils.git";
            string idEtFwUtilsGitBranch = "GitBranchEtFwUtils";
            string gitBranchEtFwUtils = "main";
            string idEtFwUtilsLocalRepo = "EtFwUtilsRepoName";
            string valEtFwUtilsLocalRepo = "EtFwUtils"; 

            string idDltViewerV1GitUrl = "GitUrlDltViewerV1";
            string gitUrlDltViewerV1 = @"https://Samaritan57@bitbucket.org/RBI-SWD/dltviewer.git";
            string idDltViewerV1GitBranch = "GitBranchDltViewerV1";
            string gitBranchDltViewerV1 = "master";
            string idDltViewerV1LocalRepo = "DltViewerV1LocalRepo";
            string valDltViewerV1LocalRepo = "DltViewerV1";

            string idDltViewerV2GitUrl = "GitUrlDltViewerV2";
            string gitUrlDltViewerV2 = @"https://Samaritan57@bitbucket.org/geoalx2/dlt_viewer.git";
            string idDltViewerV2GitBranch = "GitBranchDltViewerV2";
            string gitBranchDltViewerV2 = "master";
            string idDltViewerV2LocalRepo = "DltViewerV2LocalRepo";
            string valDltViewerV2LocalRepo = "DltViewerV2";

            string idGlobalBuildConfiguration = "GlobalBuildConfig"; 
            string globalBuildConfiguration = "NTG7";

            string password = "5pwmA2jwKnGQSNLAmu32";
            string user = "Samaritan57";


            ParametersDef GlobalBuildConfig = AddToGlobalListAndGetRef(idGlobalBuildConfiguration,
                globalBuildConfiguration, ParameterValueType.FixedValue, ref toReturn);

            ParametersDef OneShotGitUrl = AddToGlobalListAndGetRef(idOneShotGitUrl,
                gitUrlForOneShotGit, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef OneShotGitBranch = AddToGlobalListAndGetRef(idOneShotGitBranch,
                gitBranchSttOneShot, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef OneShotLocalRepo = AddToGlobalListAndGetRef(idOneShotLocalRepo,
                valOneShotLocalRepo, ParameterValueType.FixedValue, ref toReturn);

            ParametersDef EtFwUtilsGitUrl = AddToGlobalListAndGetRef(idEtFwUtilsGitUrl,
                gitUrlEtFwUtils, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef EtFwUtilsGitBranch = AddToGlobalListAndGetRef(idEtFwUtilsGitBranch,
                gitBranchEtFwUtils, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef EtFwUtilsLocalRepo = AddToGlobalListAndGetRef(idEtFwUtilsLocalRepo,
                valEtFwUtilsLocalRepo, ParameterValueType.FixedValue, ref toReturn);

            ParametersDef DltViewerV1GitUrl = AddToGlobalListAndGetRef(idDltViewerV1GitUrl,
                gitUrlDltViewerV1, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef DltViewerV1GitBranch = AddToGlobalListAndGetRef(idDltViewerV1GitBranch,
                gitBranchDltViewerV1, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef DltViewerV1LocalRepo = AddToGlobalListAndGetRef(idDltViewerV1LocalRepo,
                valDltViewerV1LocalRepo, ParameterValueType.FixedValue, ref toReturn);

            ParametersDef DltViewerV2GitUrl = AddToGlobalListAndGetRef(idDltViewerV2GitUrl,
                gitUrlDltViewerV2, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef DltViewerV2GitBranch = AddToGlobalListAndGetRef(idDltViewerV2GitBranch,
                gitBranchDltViewerV2, ParameterValueType.FixedValue, ref toReturn);
            ParametersDef DltViewerV2LocalRepo = AddToGlobalListAndGetRef(idDltViewerV2LocalRepo,
                valDltViewerV2LocalRepo, ParameterValueType.FixedValue, ref toReturn);

            string idUserGitName = "GitUser";
            ParametersDef UserGitName = AddToGlobalListAndGetRef(idUserGitName,
                user, ParameterValueType.FixedValue, ref toReturn);
            string idUserGitPsw= "GitPsw";

            ParametersDef UserGitPsw = AddToGlobalListAndGetRef(idUserGitPsw, 
                password, ParameterValueType.FixedValue, ref toReturn);
            #endregion

            string idWorkAreaName = "WorkAreaName"; 
            ParametersDef WorkAreaName = AddToGlobalListAndGetRef(
                idWorkAreaName, workAreaName,
                ParameterValueType.FixedValue, ref toReturn);

            var idTemporaryFileSubFolder = "TemporaryFolderToDeleteAfterDownload";
            ParametersDef TemporaryFileSubFolder = AddToGlobalListAndGetRef(
                idTemporaryFileSubFolder,
                @"TemporaryToDelete",
                ParameterValueType.FixedValue, ref toReturn);

            #endregion

        #region actions adding:

            #region Clean old repos
            //TempRepo deletion
            var deleteTemp = new LocationDeletion();
            deleteTemp.TargetToDelete.Value =
                $"{idTemporaryFileSubFolder}";
            deleteTemp.TargetToDelete.ValueType =
                ParameterValueType.FromGlobal;
            toReturn.BuildActions.Add(deleteTemp);
            //WorkAreaDeletion
            var deleteWA = new LocationDeletion();
            deleteWA.TargetToDelete.Value =
                $"{idWorkAreaName}";
            deleteWA.TargetToDelete.ValueType =
                ParameterValueType.FromGlobal;
            toReturn.BuildActions.Add(deleteWA);
            #endregion

            #region Download Et Fw Zip in a temp file
            var DownEtFwZip = new CopyFileToLocation();
            DownEtFwZip.SourceFile.Value = 
                $"{idFolderEtFwLibPath}" +
                $";{idEtFwZipVersion}";
            DownEtFwZip.SourceFile.ValueType = 
                ParameterValueType.PathCombination;
            DownEtFwZip.TargetPath.Value = 
                $"{idTemporaryFileSubFolder};" +
                $"{idEtFwZipVersion}"; 
            DownEtFwZip.TargetPath.ValueType = 
                ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(DownEtFwZip);
            #endregion

            #region Extract EtFw in the temporary folder  
            var unzipEtFw = new UnZipTo();
            unzipEtFw.InputZip.Value = 
                $"{idTemporaryFileSubFolder}" +
                $";{idEtFwZipVersion}";
            unzipEtFw.InputZip.ValueType = 
                ParameterValueType.PathCombination;
            unzipEtFw.OutputRepo.Value = 
                $"{idTemporaryFileSubFolder}";
            unzipEtFw.OutputRepo.ValueType = 
                ParameterValueType.FromGlobal;
            toReturn.BuildActions.Add(unzipEtFw);
            #endregion

            #region Download Thrift Zip in THE temp file
            var DownThriftZip = new CopyFileToLocation();
            DownThriftZip.SourceFile.Value =
                $"{idFolderThriftLibPath}" +
                $";{idThriftZipVersion}";
            DownThriftZip.SourceFile.ValueType =
                ParameterValueType.PathCombination;
            DownThriftZip.TargetPath.Value =
                $"{idTemporaryFileSubFolder};" +
                $"{idThriftZipVersion}";
            DownThriftZip.TargetPath.ValueType =
                ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(DownThriftZip);
            #endregion

            # region Extract Zip in Temp folder
            var unzipThrift = new UnZipTo();
            unzipThrift.InputZip.Value =
                 $"{idFolderThriftLibPath}" +
                 $";{idThriftZipVersion}";
            unzipThrift.InputZip.ValueType =
                ParameterValueType.PathCombination;
            unzipThrift.OutputRepo.Value =
                $"{idTemporaryFileSubFolder};{idThriftVersion}";
            unzipThrift.OutputRepo.ValueType =
                ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(unzipThrift);
            #endregion

            # region Move the EtFw folder to root and rename with WorkAreaname
            var moveEtFwRepo = new CopyFolderToLocation();
            moveEtFwRepo.SourceFolder.Value = 
                $"{idTemporaryFileSubFolder}" +
                $";{idEtFwVersion}";
            moveEtFwRepo.SourceFolder.ValueType =
                ParameterValueType.PathCombination;
            moveEtFwRepo.TargetPath.Value = 
                $"{idWorkAreaName}" +
                $";{idEtFwBinFolder}";
            moveEtFwRepo.TargetPath.ValueType = 
                ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(moveEtFwRepo);
            #endregion

            #region move thrift Bin into WorkAreName
            var moveThriftBin = new CopyFolderToLocation();
            moveThriftBin.SourceFolder.Value =
                $"{idTemporaryFileSubFolder};{idThriftVersion};bin";
            moveThriftBin.SourceFolder.ValueType =
                ParameterValueType.PathCombination;
            moveThriftBin.TargetPath.Value =
                $"{idWorkAreaName};{idEtFwBinFolder};bin";
            moveThriftBin.TargetPath.ValueType =
                ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(moveThriftBin); 
            #endregion

            #region rename default Ntg7 Config into default config
            var renameNtg7Conf = new CopyFolderToLocation();
            renameNtg7Conf.SourceFolder.Value =
                $"{idWorkAreaName};{idEtFwBinFolder};Profiles;NTG7_OFN_Simulation";
            renameNtg7Conf.SourceFolder.ValueType = 
                ParameterValueType.PathCombination;
            renameNtg7Conf.TargetPath.Value =
                $"{idWorkAreaName};{idEtFwBinFolder};Profiles;default";
            renameNtg7Conf.TargetPath.ValueType =
                ParameterValueType.PathCombination;

            toReturn.BuildActions.Add(renameNtg7Conf);
            #endregion

            #region add thrift config in the default profil
            var moveThriftConfig = new CopyFolderToLocation();
            moveThriftConfig.SourceFolder.Value =
                $"{idTemporaryFileSubFolder};{idThriftVersion};config";
            moveThriftConfig.SourceFolder.ValueType =
                ParameterValueType.PathCombination; 
            moveThriftConfig.TargetPath.Value =
                $"{idWorkAreaName};{idEtFwBinFolder};Profiles;default";
            moveThriftConfig.TargetPath.ValueType = 
                ParameterValueType.PathCombination;

            toReturn.BuildActions.Add(moveThriftConfig);

            #endregion

            #region In the same WA repo, add project to build functionnalities

                #region Dlt viewer V1
            var CloneDltViewer1 = new GitExport();
            CloneDltViewer1.OnlineRepository.Value = idDltViewerV1GitUrl;
            CloneDltViewer1.Branch.Value = idDltViewerV1GitBranch;

            CloneDltViewer1.OnlineRepository.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer1.Branch.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer1.Password.Value = idUserGitPsw;
            CloneDltViewer1.Password.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer1.UserName.Value = idUserGitName;
            CloneDltViewer1.UserName.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer1.LocalRepository.Value = $"{idWorkAreaName};{idDltViewerV1LocalRepo}";
            CloneDltViewer1.LocalRepository.ValueType = ParameterValueType.PathCombination; 
            toReturn.BuildActions.Add(CloneDltViewer1); 
                #endregion

                #region Dlt viewer V2
            var CloneDltViewer2 = new GitExport();
            CloneDltViewer2.OnlineRepository.Value = idDltViewerV2GitUrl;
            CloneDltViewer2.Branch.Value = idDltViewerV2GitBranch;
            CloneDltViewer2.OnlineRepository.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer2.Branch.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer2.Password.Value = idUserGitPsw;
            CloneDltViewer2.Password.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer2.UserName.Value = idUserGitName;
            CloneDltViewer2.UserName.ValueType = ParameterValueType.FromGlobal;
            CloneDltViewer2.LocalRepository.Value = $"{idWorkAreaName};{idDltViewerV2LocalRepo}";
            CloneDltViewer2.LocalRepository.ValueType = ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(CloneDltViewer2);
                #endregion

                #region EtFwUtils
            var CloneEtFwUtils = new GitExport();
            CloneEtFwUtils.OnlineRepository.Value = idEtFwUtilsGitUrl;
            CloneEtFwUtils.Branch.Value = idEtFwUtilsGitBranch;

            CloneEtFwUtils.OnlineRepository.ValueType = ParameterValueType.FromGlobal;
            CloneEtFwUtils.Branch.ValueType = ParameterValueType.FromGlobal;
            CloneEtFwUtils.Password.Value = idUserGitPsw;
            CloneEtFwUtils.Password.ValueType = ParameterValueType.FromGlobal;
            CloneEtFwUtils.UserName.Value = idUserGitName;
            CloneEtFwUtils.UserName.ValueType = ParameterValueType.FromGlobal;
            CloneEtFwUtils.LocalRepository.Value = $"{idWorkAreaName};{idEtFwUtilsLocalRepo}";
            CloneEtFwUtils.LocalRepository.ValueType = ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(CloneEtFwUtils);
                #endregion

                #region SttTest jobs and addOn
            var CloneSttProject = new GitExport();
            CloneSttProject.OnlineRepository.Value = idOneShotGitUrl;
            CloneSttProject.Branch.Value = idOneShotGitBranch;

            CloneSttProject.OnlineRepository.ValueType = ParameterValueType.FromGlobal;
            CloneSttProject.Branch.ValueType = ParameterValueType.FromGlobal;
            CloneSttProject.Password.Value = idUserGitPsw;
            CloneSttProject.Password.ValueType = ParameterValueType.FromGlobal;
            CloneSttProject.UserName.Value = idUserGitName;
            CloneSttProject.UserName.ValueType = ParameterValueType.FromGlobal;
            CloneSttProject.LocalRepository.Value = $"{idWorkAreaName};{idOneShotLocalRepo}";
            CloneSttProject.LocalRepository.ValueType = ParameterValueType.PathCombination;
            toReturn.BuildActions.Add(CloneSttProject);
            #endregion

            #endregion

            #region Build the project

            #region Dlt V1:
            //var buildDltViewer1 = new BuildProject();
            //buildDltViewer1.Configuration.Value = idGlobalBuildConfiguration;
            //buildDltViewer1.Configuration.ValueType = ParameterValueType.FromGlobal;
            //buildDltViewer1.Platform.Value = "Any CPU";
            //buildDltViewer1.Platform.ValueType = ParameterValueType.FixedValue;
            //buildDltViewer1.ProjectPath.Value = 
            //    $"{idWorkAreaName};{idDltViewerV1LocalRepo};DltViewer;DltViewer.sln";
            //buildDltViewer1.ProjectPath.ValueType = ParameterValueType.PathCombination;
            //toReturn.BuildActions.Add(buildDltViewer1);
            #endregion

            #region DLT V2: 


            #endregion

            #endregion



            #region (Deactivated for now) clean temp folder
            //TempRepo deletion
            var delteTemp2 = new LocationDeletion();
            delteTemp2.TargetToDelete.Value =
                $"{idTemporaryFileSubFolder}";
            delteTemp2.TargetToDelete.ValueType =
                ParameterValueType.FromGlobal;
            //toReturn.BuildActions.Add(delteTemp2);
            #endregion

        #endregion
            return toReturn;
        }
    }
}
