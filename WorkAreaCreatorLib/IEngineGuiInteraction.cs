﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkAreaCreatorLib
{
    //enum used for the notify method
    public enum LogLevel
    {
        INFO,
        WARNING,
        ERROR,
        FAILURE,
        SUCCESS,
        OPTION
    };

    public interface IEngineGuiInteraction
    {
        //each constructor must pass this method in order to get the WACreator Control
        void Initialize(WACreatorController Controller);
        void Notify(LogLevel level, string text);

        // These methods are used to request the gui to perform action in order to fixed issue that engine can not solve itself
        bool RequestUserToChangePath(string msg, ref string path);
        bool RequestUserToUnlockFile(string message);
    }
}
