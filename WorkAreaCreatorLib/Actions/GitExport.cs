﻿using System;
using LibGit2Sharp;

namespace WorkAreaCreatorLib.Configs
{
    public class GitExport: ExecutableAction
    {

        public const string IdUserName ="UserName";
        public ParametersDef UserName
        {
            get { return this.ParametersDef[IdUserName]; }
            set { this.ParametersDef[IdUserName] = value; }
        }

        public const string IdPassword = "Password";
        public ParametersDef Password
        {
            get { return this.ParametersDef[IdPassword]; }
            set { this.ParametersDef[IdPassword] = value; }
        }

        public const string IdBranch = "Branch";
        public ParametersDef Branch
        {
            get { return this.ParametersDef[IdBranch]; }
            set { this.ParametersDef[IdBranch] = value; }
        }

        public const string IdOnlineRepository = "OnlineRepository";
        public ParametersDef OnlineRepository
        {
            get { return this.ParametersDef[IdOnlineRepository]; }
            set { this.ParametersDef[IdOnlineRepository] = value; }
        }

        public const string IdLocalRepository = "LocalRepository";
        public ParametersDef LocalRepository
        {
            get { return this.ParametersDef[IdLocalRepository]; }
            set { this.ParametersDef[IdLocalRepository] = value; }
        }

        public GitExport()
        {
            this.ParametersDef.Add(IdUserName, new ParametersDef(IdUserName, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdPassword , new ParametersDef(IdPassword, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdBranch , new ParametersDef(IdBranch, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdOnlineRepository , new ParametersDef(IdOnlineRepository, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdLocalRepository , new ParametersDef(IdLocalRepository, ParameterValueType.FixedValue));
        }
        
        protected override bool Exec()
        {
            FileManipulationHelper fmHelper = new FileManipulationHelper();
            fmHelper.Info += OnInfo;

            var result = GitExportFct(UserName.Value, Password.Value, ref OnlineRepository.Value,
                ref LocalRepository.Value, ref Branch.Value, new FileManipulationHelper());

            fmHelper.Info -= OnInfo;
            return result;
            
        }

        public override string GetDescription()
        {
            return "Will clone a git repository to the wanted local path";
        }

        public override string[] GetParameters()
        {
            return new string[] 
            {
                $"{IdUserName}: User name to present to access repo.",
                $"{IdPassword}: This user password.",
                $"{IdBranch}: The Git branch to load.",
                $"{IdOnlineRepository}: The link where to find the git repo.",
                $"{IdLocalRepository}: The location where to download the project."
            };
        }


        private bool GitExportFct(string user, string password,
            ref string gitProjectLink, ref string localDrivePath, 
            ref string gitBranch, FileManipulationHelper helper)
        {
            try
            {
                helper.CloneProjectAndSetUpBranche(
                    localDrivePath, gitProjectLink,
                    gitBranch, password, user);
                return true;
            }
            catch (System.IO.IOException ioE)
            {
                if (GuiInteraction.RequestUserToUnlockFile(String.Format("Not able to access the file {0}. \n" +
            "Error description:\n {1}. Please unlock the file. \n", localDrivePath, ioE)))
                    return GitExportFct(user, 
                        password,
                        ref gitProjectLink,
                        ref localDrivePath,
                        ref gitBranch,
                        helper);
                else
                {
                    if (GuiInteraction.RequestUserToChangePath("Would you like to try to change the target path?" +
                        $"Curently is {localDrivePath}", ref localDrivePath))
                        return GitExportFct(user, password,
                        ref gitProjectLink,
                        ref localDrivePath,
                        ref gitBranch,
                        helper);
                    else
                    {
                        OnError(this, "Action cancelled by user. °_°");
                        return false;
                    }
                }
            }
            catch (LibGit2SharpException lg2sE)
            {
                OnError(this, "Something went wrong with the content of the config file." +
                    " Can not clone with following objects.\n" +
                    $" Clone instruction details:\n" +
                    $"     - git link: {gitProjectLink}\n" +
                    $"     - local drive Path: {localDrivePath}\n" +
                    $"     - git branch: {gitBranch}\n" +
                    $" User sesion detail: \n" +
                    $"     - user: {user}\n" +
                    $"     - password: {password}\n" +
                    $"\n\n The following exception was thrown:\n{lg2sE}\n ");
                return false;
            }
            catch (NullReferenceException nullE)
            {
                OnError(this, $":\nSeems that the wanted branch was not find for:\n" +
                    $" Clone instruction details:\n" +
                    $"     - git_remote_path: {gitProjectLink}\n" +
                    $"     - local_drive_destination: {localDrivePath}\n" +
                    $"     - branch: {gitBranch}\n" +
                    $" User sesion detail: \n" +
                    $"     - user: {user}\n" +
                    $"     - password: {password}\n" +
                    $"Exception details: \n{nullE} \n");
                return false;
            }
        }
    }
}

