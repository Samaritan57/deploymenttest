﻿using System;

using System.IO;
using System.IO.Compression;
using System.Security.Principal;
using System.Security.AccessControl;

namespace WorkAreaCreatorLib.Configs
{
    /// <summary>
    /// Action class permitting to extract some zip formated file 
    /// InputZip: The location of ZIP file to extract
    /// OutputRepo: Target where to save the file.
    /// </summary>
    public class UnZipTo : ExecutableAction
    {

        public const string IdInputZip = "InputZip";
        public ParametersDef InputZip
        {
            get { return this.ParametersDef[IdInputZip]; }
            set { this.ParametersDef[IdInputZip] = value; }
        }

        public const string IdOutputRepo = "OutputRepo";
        public ParametersDef OutputRepo
        {
            get { return this.ParametersDef[IdOutputRepo]; }
            set { this.ParametersDef[IdOutputRepo] = value; }
        }

        public UnZipTo()
        {
            this.ParametersDef.Add(IdInputZip, new ParametersDef(IdInputZip, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdOutputRepo, new ParametersDef(IdOutputRepo, ParameterValueType.FixedValue));

        }

        protected override bool Exec()
        {
            return UnzipTargetDirectory();
        }

        /// <summary>
        /// Unzip given zipPath to a directory (creates a new directory if it doens exist)
        /// </summary>
        /// <param name="zipPath"></param>
        /// <param name="extractDirectory"></param>
        private bool UnzipTargetDirectory()
        {
            OnInfo(this, String.Format("Un-Zip START from:'{0}' to:'{1}'"
                , InputZip.Value, OutputRepo.Value));
            DirectoryInfo source;
            DirectoryInfo target;
            try
            {
                if (!File.Exists(InputZip.Value))
                    throw new Exception($"There is no file at path {InputZip.Value}...");
                source = new DirectoryInfo(InputZip.Value);
                try
                {
                    if (!Directory.Exists(OutputRepo.Value))
                        Directory.CreateDirectory(OutputRepo.Value); 

                    target = new DirectoryInfo(OutputRepo.Value);
                    UnZipProccess(source, target);
                    OnInfo(this, String.Format("Un-Zip DONE from:'{0}' to:'{1}'"
                        , InputZip.Value, OutputRepo.Value));
                    return true;
                }
                catch (IOException e)
                {
                    if (GuiInteraction.RequestUserToChangePath(
                        String.Format($"Can not access the target repo folder at " +
                        $"path: '{OutputRepo.Value}'.\nExceptionDetails: {e}")
                       , ref OutputRepo.Value))
                    {
                        return UnzipTargetDirectory();
                    }
                    else {
                        OnError(this, String.Format($"Can not access the target repo folder at " +
                        $"path: '{OutputRepo.Value}'.\nExceptionDetails: {e}")); 
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OnError(this, "Not capable of unzip the file. " +
                        $"\n Issue description:\n{e}");
                    return false;
                }
            }
            catch (Exception e)
            {
                if (GuiInteraction.RequestUserToChangePath(String.Format("Can not find the zip file at " +
                    $"path: '{InputZip.Value}'.  \nExceptionDetails: {e}")
                   , ref InputZip.Value))
                {
                    return UnzipTargetDirectory(); 
                }
                else return false;
            }
        }

        /// <summary>
        /// deletes target Directory
        /// </summary>
        /// <param name="target_dir"></param>
        private void DeleteDirectory(string target_dir)
        {
            OnInfo(this, $"DeleteDirectory START at:{target_dir}");
            //List file in the repository to clean
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            //Set Normal right (Write, read, delete) to all elements and delete them
            foreach (string file in files)
            {
                try
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }
                catch (Exception e)
                {
                    OnInfo(this, $"Set the full user right for '{file}' cause of " +
                        $"following exception: {e}");
                    //Add to currentDirectory Write access
                    DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(file));
                    DirectorySecurity dSecurity = dirInfo.GetAccessControl();
                    // Add the FileSystemAccessRule to the security settings. 
                    dSecurity.AddAccessRule(
                        new FileSystemAccessRule(
                            new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                            FileSystemRights.FullControl,
                            InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                            PropagationFlags.InheritOnly, AccessControlType.Allow));
                    dSecurity.AddAccessRule(
                        new FileSystemAccessRule(
                            WindowsIdentity.GetCurrent().Name,
                            FileSystemRights.FullControl,
                            InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                            PropagationFlags.InheritOnly, AccessControlType.Allow));
                    // Set the access control
                    dirInfo.SetAccessControl(dSecurity);

                    //Remove the file readOnly flag
                    var fileInfo = new FileInfo(file);
                    fileInfo.IsReadOnly = false;
                    fileInfo.Attributes = FileAttributes.Normal;
                    fileInfo.Delete();
                }
            }

            //finally delete the sub folder in this local folder (recursive call)
            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }
            //And delete 
            Directory.Delete(target_dir, true);
            OnInfo(this, $"DeleteDirectory DONE at:{target_dir}");
        }
        
        private void UnZipProccess(DirectoryInfo zipPath, DirectoryInfo extractDirectory)
        {
            //NOTE: the method throws exception when zipath is invalid
            ZipFile.ExtractToDirectory(zipPath.FullName, extractDirectory.FullName);
        }

        public override string GetDescription()
        {
            return "Is unziping the 'InputZip' into the wanted 'OutputRepo'sitory"; 
        }

        public override string[] GetParameters()
        {
            return new string[] {
                $"{IdInputZip}: Path of the Zip to unzip.",
                $"{IdOutputRepo}: Path where the unzip file has to be store."
            };
        }
    }
}
