﻿using System;
using System.IO;


namespace WorkAreaCreatorLib.Configs
{

    public class CopyFolderToLocation : ExecutableAction
    {
        public const string IdSourceFolder= "SourceFolder";
        public ParametersDef SourceFolder
        {
            get { return this.ParametersDef[IdSourceFolder]; }
            set { this.ParametersDef[IdSourceFolder] = value; }
        }

        public const string IdTargetPath = "TargetPath";
        public ParametersDef TargetPath
        {
            get { return this.ParametersDef[IdTargetPath]; }
            set { this.ParametersDef[IdTargetPath] = value; }
        }

        public CopyFolderToLocation()
        {
            this.ParametersDef.Add(IdSourceFolder, new ParametersDef
                (IdSourceFolder, ParameterValueType.FixedValue));
            this.ParametersDef.Add(IdTargetPath, new ParametersDef
                (IdTargetPath, ParameterValueType.FixedValue));

        }

        #region Action description
        public override string GetDescription()
        {
            return "Will copy a folder and its sub-repository into the wanted local location";
        }
        public override string[] GetParameters()
        {
            return new string[]
            {
            "SourcePath: Location of the folder to download.",
            "TargetPath: The place were to copy the folder"
            };
        }
        #endregion

        //Function becoming recursive if user is providing correction when an error is detected. 
        private bool CopyFolder(ref string sourceFolder, ref string target
            , FileManipulationHelper helper, IEngineGuiInteraction GuiInteract)
        {
            try
            {
                helper.CopyFolder(sourceFolder, target);
                return true;
            }
            catch (DirectoryNotFoundException dnfe)
            {
                if (!Directory.Exists(sourceFolder))
                    if (GuiInteract.RequestUserToChangePath(String.Format
                        ("Was not able to find the source folder at path, '{0}', following exception" +
                        " occured: \n{1}", sourceFolder, dnfe), ref sourceFolder))
                    {
                        return CopyFolder(ref sourceFolder, ref target, helper, GuiInteract);
                    }
                    else
                    {
                        OnError(this, String.Format
                        ("Was not able to find the source folder at path, '{0}', following exception" +
                        " occured: \n{1}", sourceFolder, dnfe));
                        return false;
                    }
                else
                    if (GuiInteract.RequestUserToChangePath(String.Format
                        ("Was not able to access local path, '{0}', following exception" +
                        " occured: \n{1}", target, dnfe), ref target))
                    {
                        return CopyFolder(ref sourceFolder, ref target, helper, GuiInteract);
                    }
                    else
                        return false;

            }
            catch (IOException ioE)
            {
                if (GuiInteract.RequestUserToUnlockFile(String.Format("Not able to copy folder at " +
                    "path {0}. Target path seems to be locked. Or the path need to be changed\n" +
                   "Error description:\n {1}. " +
                   "\nCan you unlock the path ?", target, ioE)))
                {
                    return CopyFolder(ref sourceFolder, ref target, helper, GuiInteract);
                }
                else

                {
                    if (GuiInteract.RequestUserToChangePath("Would you like to change the " +
                        "path then?\n" + $"current folder target path is: {target}.", ref target))
                    {
                        return CopyFolder(ref sourceFolder, ref target, helper, GuiInteract);
                    }
                    else
                    {
                        OnError(this, "Action cancelled by user");
                        GuiInteraction.Notify(LogLevel.FAILURE, $"Not able to copy at target path:" +
                            $"{target}");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                OnInfo(this, String.Format("Environement is not load properly !" +
                    "Can not copy the file '{0}' to target folder '{1}' " +
                    "\nException details:" +
                    "\n{3}", sourceFolder, target, e));
                return false;
            }
        }

        protected override bool Exec()
        {
            FileManipulationHelper fmHelper = new FileManipulationHelper();
            fmHelper.Info += OnInfo;
            var result = CopyFolder(ref SourceFolder.Value, ref TargetPath.Value, fmHelper, GuiInteraction);
            fmHelper.Info -= OnInfo;
            return result;
        }
    }

    
}
