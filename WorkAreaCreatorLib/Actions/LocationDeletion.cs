﻿using System;
using System.IO;
using WorkAreaCreatorLib.Configs; 

namespace WorkAreaCreatorLib.Configs
{
    public class LocationDeletion : ExecutableAction
    {
        public const string IdTargetToDelete = "TargetToDelete";
        public ParametersDef TargetToDelete
        {
            get { return this.ParametersDef[IdTargetToDelete]; }
            set { this.ParametersDef[IdTargetToDelete] = value; }
        }

        public LocationDeletion()
        {
            this.ParametersDef.Add(IdTargetToDelete, new ParametersDef
                (IdTargetToDelete, ParameterValueType.FixedValue)); 
        }

        private bool CleanProcess(string targToDelete, FileManipulationHelper helper
            , IEngineGuiInteraction guiInteract)
        {
            try
            {
                if (Directory.Exists(targToDelete))
                    helper.DeleteFolder(targToDelete);
                return true;
            }
            catch (FileNotFoundException)
            {
                OnInfo(this, String.Format("No need to delete the file '{0}', this one can't" +
                    "be found.\n", targToDelete));
                return true;
            }
            catch (DirectoryNotFoundException)
            {
                OnInfo(this, String.Format("No need to delete the folder '{0}', this one can't" +
                    "be found.\n", targToDelete));
                return true;
            }
            catch (IOException ioE)
            {
                if (guiInteract.RequestUserToUnlockFile(String.Format("Not able to access a file in the folder {0}." +
                    "\nError description:\n {1}\n", targToDelete, ioE)))
                    return CleanProcess(targToDelete, helper, guiInteract);
                else
                {
                    OnInfo(this, "Action cancelled by user\n");
                    return false;
                }
            }
            catch (Exception e)
            {
                OnInfo(this, String.Format("Environement is not load properly !" +
                    "Can not delete the folder because of the following exception. " +
                    "\nException: " +
                    "\n{0}\n", e));
                return false;
            }
        }

        protected override bool Exec()
        {
            FileManipulationHelper fmHelper = new FileManipulationHelper();
            fmHelper.Info += OnInfo;
            var result = CleanProcess(TargetToDelete.Value, fmHelper, GuiInteraction);
            fmHelper.Info -= OnInfo;
            return result;
        }

        public override string GetDescription()
        {
            return "Will copy a file into the wanted path taget";
        }

        public override string[] GetParameters()
        {
            return new string[] {$"{IdTargetToDelete}: The path of the file or folder to delete."};
        }
    }
}
