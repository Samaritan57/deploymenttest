﻿using System;
using System.IO; 
using System.Collections.Generic;

using Microsoft.Build.Execution;
using Microsoft.Build.Evaluation;

using Microsoft.Build.Utilities;
using Microsoft.Build.Framework;
using System.Security;

namespace WorkAreaCreatorLib.Configs
{

    public class BuildProject : ExecutableAction
    {
        public const string IdProjectPath = "ProjectPath";
        public ParametersDef ProjectPath
        {
            get { return this.ParametersDef[IdProjectPath]; }
            set { this.ParametersDef[IdProjectPath]=value; }
        }

        public const string IdConfiguration = "Configuration";
        public ParametersDef Configuration
        {
            get { return this.ParametersDef[IdConfiguration]; }
            set { this.ParametersDef[IdConfiguration] = value; }
        }

        public const string IdPlatform = "Platform";
        public ParametersDef Platform
        {
            get { return this.ParametersDef[IdPlatform]; }
            set { this.ParametersDef[IdPlatform] = value; }
        }

        public BuildProject()
        {
            //List element in parameters def to permit there update afterwards via the params update option
            this.ParametersDef.Add(IdProjectPath, new ParametersDef(
                IdProjectPath, ParameterValueType.FixedValue ));

            this.ParametersDef.Add(IdConfiguration, new ParametersDef(
                IdConfiguration, ParameterValueType.FixedValue));

            this.ParametersDef.Add(IdPlatform, new ParametersDef(
                IdPlatform, ParameterValueType.FixedValue));
        }

        protected override bool Exec()
        {
            OnInfo(this, $"Try to build the project at {IdProjectPath}: " +
                $"'{ProjectPath.Value}', with {IdConfiguration}: " +
                $"'{Configuration.Value}' and for {IdPlatform}: {Platform.Value}");
            return BuildProjectFct(ProjectPath.Value, Configuration.Value, Platform.Value);
        }

        public override string GetDescription()
        {
            return "Is gonna construct the project according the given params: " +
                "'ProjectPath, 'Configuration', 'Platform'"; 
        }

        public override string[] GetParameters()
        {
            return new string[] {
                $"{ProjectPath}: The project path on the local drive.",
                $"{Configuration}: Specific to the project, could be 'DEBUB','RELEASE', 'NTG7'...",
                $"{Platform}: Basically the CPU architecture, 'Any CPU', 'x64', 'x32'..." 
            };
        }

        private bool BuildProjectFct(string projectFullPath, string configuration, string platform)
        {
            OnInfo(this, $"Start building project now: " +
                $"\n ProjectFullPath: {projectFullPath}" +
                $"\n Configuration: {configuration}" +
                $"\n Platform: {platform}"); 
            var properties = new SerializableDictionary<String, String>();
            properties.Add("Configuration", configuration); //<--- change here
            properties.Add("Platform", platform);//<--- change here

            BasicLogger Logger = new BasicLogger();
            var projectCollection = new ProjectCollection();


            var buildParamters = new BuildParameters(projectCollection);
            buildParamters.Loggers = new List<Microsoft.Build.Framework.ILogger>() { Logger };

            BuildManager.DefaultBuildManager.ResetCaches();

            var buildRequest = new BuildRequestData(projectFullPath, properties, "4.0", new String[] { "Build" }, null);
            var buildResult = BuildManager.DefaultBuildManager.Build(buildParamters, buildRequest);

            OnInfo(this, Logger.GetLogString());

            if (buildResult.OverallResult == BuildResultCode.Failure)
            {
                var errMsg = "Not able to build properly project now: " +
                $"\n ProjectFullPath: {projectFullPath}" +
                $"\n Configuration: {configuration}" +
                $"\n Platform: {platform}";

                OnError(this, errMsg);

                GuiInteraction.Notify(LogLevel.FAILURE, errMsg);
                return false;
            }
            return true;
        }

        #region Logger for the code compiler 
        public class BasicLogger : Logger
        {
            MemoryStream streamMem = new MemoryStream();

            /// <summary>
            /// Initialize is guaranteed to be called by MSBuild at the start of the build
            /// before any events are raised.
            /// </summary>
            public override void Initialize(IEventSource eventSource)
            {
                try
                {
                    // Open the file
                    this.streamWriter = new StreamWriter(streamMem);
                    //this.streamWriter = new StreamWriter(logFile);
                }
                catch (Exception ex)
                {
                    if
                    (
                        ex is UnauthorizedAccessException
                        || ex is ArgumentNullException
                        || ex is PathTooLongException
                        || ex is DirectoryNotFoundException
                        || ex is NotSupportedException
                        || ex is ArgumentException
                        || ex is SecurityException
                        || ex is IOException
                    )
                    {
                        throw new LoggerException("Failed to create log file: " + ex.Message);
                    }
                    else
                    {
                        // Unexpected failure
                        throw;
                    }
                }

                // For brevity, we'll only register for certain event types. Loggers can also
                // register to handle TargetStarted/Finished and other events.
                eventSource.ProjectStarted += 
                    new ProjectStartedEventHandler(eventSource_ProjectStarted);
                eventSource.TaskStarted += 
                    new TaskStartedEventHandler(eventSource_TaskStarted);
                eventSource.MessageRaised += 
                    new BuildMessageEventHandler(eventSource_MessageRaised);
                eventSource.WarningRaised += 
                    new BuildWarningEventHandler(eventSource_WarningRaised);
                eventSource.ErrorRaised += 
                    new BuildErrorEventHandler(eventSource_ErrorRaised);
                eventSource.ProjectFinished += 
                    new ProjectFinishedEventHandler(eventSource_ProjectFinished);
            }

            void eventSource_ErrorRaised(object sender, BuildErrorEventArgs e)
            {
                // BuildErrorEventArgs adds LineNumber, ColumnNumber, File, amongst other parameters
                string line = String.Format(": ERROR {0}({1},{2}): ", e.File, e.LineNumber, e.ColumnNumber);
                WriteLineWithSenderAndMessage(line, e);
            }

            void eventSource_WarningRaised(object sender, BuildWarningEventArgs e)
            {
                // BuildWarningEventArgs adds LineNumber, ColumnNumber, File, amongst other parameters
                string line = String.Format(": Warning {0}({1},{2}): ", e.File, e.LineNumber, e.ColumnNumber);
                WriteLineWithSenderAndMessage(line, e);
            }

            void eventSource_MessageRaised(object sender, BuildMessageEventArgs e)
            {
                // BuildMessageEventArgs adds Importance to BuildEventArgs
                // Let's take account of the verbosity setting we've been passed in deciding whether to log
                // the message
                if ((e.Importance == MessageImportance.High && IsVerbosityAtLeast(LoggerVerbosity.Minimal))
                    || (e.Importance == MessageImportance.Normal && IsVerbosityAtLeast(LoggerVerbosity.Normal))
                    || (e.Importance == MessageImportance.Low && IsVerbosityAtLeast(LoggerVerbosity.Detailed))
                )
                {
                    WriteLineWithSenderAndMessage(String.Empty, e);
                }
            }

            void eventSource_TaskStarted(object sender, TaskStartedEventArgs e)
            {
                // TaskStartedEventArgs adds ProjectFile, TaskFile, TaskName
                // To keep this log clean, this logger will ignore these events.
            }

            void eventSource_ProjectStarted(object sender, ProjectStartedEventArgs e)
            {
                // ProjectStartedEventArgs adds ProjectFile, TargetNames
                // Just the regular message string is good enough here, so just display that.
                WriteLine(String.Empty, e);
                indent++;
            }

            void eventSource_ProjectFinished(object sender, ProjectFinishedEventArgs e)
            {
                // The regular message string is good enough here too.
                indent--;
                WriteLine(String.Empty, e);
            }


            /// <summary>
            /// Write a line to the log, adding the SenderName and Message
            /// (these parameters are on all MSBuild event argument objects)
            /// </summary>
            private void WriteLineWithSenderAndMessage(string line, BuildEventArgs e)
            {
                if (0 == String.Compare(e.SenderName, "MSBuild", true /*ignore case*/))
                {
                    // Well, if the sender name is MSBuild, let's leave it out for prettiness
                    WriteLine(line, e);
                }
                else
                {
                    WriteLine(e.SenderName + ": " + line, e);
                }
            }


            /// <summary>
            /// Just write a line to the log
            /// </summary>
            private void WriteLine(string line, BuildEventArgs e)
            {
                for (int i = indent; i > 0; i--)
                {
                    streamWriter.Write("\t");
                }
                streamWriter.Write(line + e.Message);
            }

            public string GetLogString()
            {
                var sr = new StreamReader(streamMem);
                var myStr = sr.ReadToEnd();
                return myStr;
            }

            /// <summary>
            /// Shutdown() is guaranteed to be called by MSBuild at the end of the build, after all
            /// events have been raised.
            /// </summary>
            public override void Shutdown()
            {
                streamWriter.Flush();
                streamMem.Position = 0;
            }

            private StreamWriter streamWriter;
            private int indent;

        }
        #endregion

    }
}
