﻿using System;
using System.IO; 


namespace WorkAreaCreatorLib.Configs
{
    public class SvnExport : ExecutableAction
    {
        public const string IdSvnRemotePath = "SvnRemotePath";
        public ParametersDef SvnRemotePath
        {
            get { return this.ParametersDef[IdSvnRemotePath]; }
            set { this.ParametersDef[IdSvnRemotePath] = value; }
        }

        public const string IdLocalDrivePath = "LocalDrivePath";
        public ParametersDef LocalDrivePath
        {
            get { return this.ParametersDef[IdLocalDrivePath]; }
            set { this.ParametersDef[IdLocalDrivePath] = value; }
        }

        public SvnExport()
        {

            ParametersDef.Add(IdSvnRemotePath, new ParametersDef
                (IdSvnRemotePath, ParameterValueType.FixedValue));
            ParametersDef.Add(IdLocalDrivePath, new ParametersDef
                (IdLocalDrivePath, ParameterValueType.FixedValue));
        }

        private bool Export(ref string svnRemotePath, ref string localDrivePath,
            FileManipulationHelper helper, IEngineGuiInteraction guiInteract)
        {
            try
            {
                helper.CopyFilesFromSVN(svnRemotePath, localDrivePath);
                return true;
            }
            catch (IOException ioE)
            {
                if (guiInteract.RequestUserToChangePath(String.Format("Can not copy data" +
                    " from svn PATH '{0}', following exception occured: \n{1}\n", svnRemotePath, ioE)
                    , ref svnRemotePath))
                {
                    return Export(ref svnRemotePath, ref localDrivePath, helper, guiInteract);
                }
                else return false;
            }
            catch (SystemException sE)
            {
                if (guiInteract.RequestUserToUnlockFile(String.Format("Not able to access a file at " +
                    "path {0}\n Error description:\n {1}\n", svnRemotePath, sE)))
                    return Export(ref svnRemotePath, ref localDrivePath, helper, guiInteract);
                else
                {
                    OnInfo(this, "Action cancelled by user\n");
                    return false;
                }
            }
            catch (Exception e)
            {
                if (guiInteract.RequestUserToChangePath("Can not copy the file at LocalPath: {0}."+
                    "Would you like to try to chan ge the Local path?", ref localDrivePath))
                {
                    return Export(ref SvnRemotePath.Value, ref localDrivePath, helper, guiInteract);
                }
                OnInfo(this, "Environement is not load properly !" +
                    $"Can not copy the repository from '{svnRemotePath}' to '{localDrivePath}'" +
                    $"\nException: \n{e}" +
                    "\nAction was cancelled by user.");
                return false;
            }
        }

        protected override bool Exec()
        {
            FileManipulationHelper fmHelper = new FileManipulationHelper();
            fmHelper.Info += OnInfo;
            var result = Export(ref SvnRemotePath.Value, ref LocalDrivePath.Value, fmHelper, GuiInteraction); 
            fmHelper.Info -= OnInfo;
            return result;
        }

        public override string GetDescription()
        {
            return "Will a repository from the svn into the wanted local path target";
        }

        public override string[] GetParameters()
        {
            return new string[] { $"{IdSvnRemotePath}: The svn path where to find the project."
            ,$"{LocalDrivePath.Value}: The path where to save the project. It's better to set it with a " +
            "relative path. "};

        }


    }
}
