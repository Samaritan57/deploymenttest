﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization; 

namespace WorkAreaCreatorLib.Configs
{
    /// <summary>
    ///  Get a Info and Error event for the logging
    ///  
    ///  You need to initialise the action by passing the IEngineGuiInteraction
    ///  This way user based logic is gona connect to the action
    ///  
    ///  Action can be execute if IsInit return true (Happens normally when Initialized is done). 
    /// 
    ///  Action execution can be command by calling execute ()
    /// </summary>
    [XmlInclude(typeof(BuildProject))]
    [XmlInclude(typeof(CopyFileToLocation))]
    [XmlInclude(typeof(CopyFolderToLocation))]
    [XmlInclude(typeof(GitExport))]
    [XmlInclude(typeof(LocationDeletion))]
    [XmlInclude(typeof(SvnExport))]
    [XmlInclude(typeof(UnZipTo))]
    public abstract class ExecutableAction
    {
        #region event based logging 
        public event EventHandler<string> Info;
        protected void OnInfo(object caller, string message)
        { 
            this.Info?.Invoke(caller, message);
            if (IsInit) GuiInteraction.Notify(LogLevel.INFO, message); 
        }

        public event EventHandler<string> Error;
        protected void OnError(object caller, string message)
        { 
            this.Error?.Invoke(caller, message);
            if (IsInit) GuiInteraction.Notify(LogLevel.ERROR, message);
        }
        #endregion 

        bool IsInit = false;

        public SerializableDictionary<string, ParametersDef> ParametersDef = 
            new SerializableDictionary<string, ParametersDef>();
        protected IEngineGuiInteraction GuiInteraction;

        public void Initialized(IEngineGuiInteraction guiInteraction)
        {
            this.GuiInteraction = guiInteraction;
            IsInit = true; 
        }

        public bool Execute()
        {
            if (IsInit)
                return Exec(); 
            else
                throw new Exception("The acion need to be init before to be execute. " +
                    "Please review your code...");
        }
        protected abstract bool Exec(); 

        public abstract string GetDescription();
        public abstract string[] GetParameters();

    }


}
