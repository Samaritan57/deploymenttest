﻿using System;
using System.IO;
namespace WorkAreaCreatorLib.Configs
{
    public class CopyFileToLocation : ExecutableAction
    {
        public const string IdSourceFile = "SourceFile"; 
        public ParametersDef SourceFile
        {
            get { return this.ParametersDef[IdSourceFile]; }
            set { this.ParametersDef[IdSourceFile] = value; }
        }

        public const string IdTargetPath = "TargetPath";
        public ParametersDef TargetPath
        {
            get { return this.ParametersDef[IdTargetPath]; }
            set { this.ParametersDef[IdTargetPath] = value; }
        }

        public CopyFileToLocation()
        {

            //List element in parameters def to permit there update afterwards via the params update option
            this.ParametersDef.Add(IdSourceFile, 
                new ParametersDef(IdSourceFile, ParameterValueType.FixedValue));

            this.ParametersDef.Add(IdTargetPath,
                new ParametersDef(IdTargetPath, ParameterValueType.FixedValue));
        }

        private bool CopyFile(ref string sourceFile, ref string targetPath, 
            FileManipulationHelper helper, IEngineGuiInteraction guiInteract)
        {
            try
            {
                helper.CopyFile(sourceFile, targetPath);
                GuiInteraction.Notify(LogLevel.SUCCESS, $"Sucessfully copy the file " +
                    $"'{Path.GetFullPath(sourceFile)}' to '{Path.GetFullPath(targetPath)}'");
                return true;
            }
            catch (FileNotFoundException fnfe)
            {
                string msg = String.Format("Was not able to find the file at path, '{0}'" +
                    ", following exception occured: \n{1}", sourceFile, fnfe);
                if (guiInteract.RequestUserToChangePath(msg, ref sourceFile))
                {
                    return CopyFile(ref sourceFile, ref targetPath, helper, guiInteract);
                }
                else
                {
                    OnInfo(this, "Action cancelled by user");
                    GuiInteraction.Notify( LogLevel.FAILURE, "Was not able to find the " +
                    $"file at path: {sourceFile}");
                    return false;
                }
            }
            catch (IOException ioE)
            {
                if (guiInteract.RequestUserToUnlockFile(String.Format("Not able to create file at path {0}. " +
                    "Target path seems to be locked. Or the path need to be changed\n" +
                   "Error description:\n {1}. " +
                   "\nCan you unlock the path ?", targetPath, ioE)))
                {
                    return CopyFile(ref sourceFile, ref targetPath, helper, guiInteract);
                }
                else
                {
                    if (guiInteract.RequestUserToChangePath("Would you like to change the path then?\n" +
                        $"current target path is {targetPath}.", ref targetPath))
                    {
                        return CopyFile(ref sourceFile, ref targetPath, helper, guiInteract);
                    }
                    else
                    {
                        OnError(this, "Action cancelled by user");
                        GuiInteraction.Notify(LogLevel.FAILURE, $"Not able to copy at target path:{targetPath}");
                        return false;
                    }
                        
                }
            }
            catch (Exception e)
            {
                OnError(this, String.Format("Environement is not load properly !" +
                    "Can not copy the file '{0}' to target '{1}' " +
                    "\nException: " +
                    "\n{3}", sourceFile, targetPath, e));
                GuiInteraction.Notify(LogLevel.FAILURE, String.Format("Environement is not load properly! " +
                    "Can not copy the file '{0}' to target '{1}' " +
                    "\nException: " +
                    "\n{3}", sourceFile, targetPath, e));
                return false;
            }
        }

        protected override bool Exec()
        {
            FileManipulationHelper fmHelper = new FileManipulationHelper();
            fmHelper.Info += OnInfo;
            var result = CopyFile(ref SourceFile.Value, ref TargetPath.Value, fmHelper, GuiInteraction);
            fmHelper.Info -= OnInfo;
            return result;
            
        }

        public override string GetDescription()
        {
            return "Will copy a file into the wanted path taget";
        }

        public override string[] GetParameters()
        {
            return new string[] {"SourceFile: Is the path of the file to copy, need to include the" +
                " file name & its extension too!"
                , "TargetPath: Is the destination of the file to copy, need to include the file" +
                " name & its extension too!" };
        }
    }
}
