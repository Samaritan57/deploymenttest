﻿using SharpSvn;
using System;
using System.IO;
using System.IO.Compression;

using System.Security.Principal;
using System.Security.AccessControl;

using LibGit2Sharp; 


namespace WorkAreaCreatorLib
{
    /// <summary>
    /// This helper is containing:
    ///  - File manipulation fct
    ///     
    ///  - Compression & Decompression  
    /// </summary>
    public class FileManipulationHelper
    {
        public event EventHandler<string> Info;

        /// <summary>
        /// Can throw "FileNotFoundException" if the source file is not available.
        /// can throw "IOException" if the target file is not editable, "Overwritable"
        /// </summary>
        /// <param name="source"></param>
        /// <param name="targetFolder"></param>
        public void CopyFile(string source, string targetFolder)
        {
            Info?.Invoke(this, String.Format("Start copying from. '{0}' to '{1}'"
                   , source, targetFolder));
            if (!Directory.Exists(Path.GetDirectoryName(targetFolder)))
                Directory.CreateDirectory(Path.GetDirectoryName(targetFolder));

            File.Copy(source, targetFolder, true);
        }

        /// <summary>
        /// Can throw "DirectoryNotFoundException" if the source folder is not available.
        /// can throw "IOException" if the target file is not editable, "Overwritable"
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="targetDirectory"></param>
        public void CopyFolder(string sourceDirectory, string targetDirectory)
        {
            Info?.Invoke(this, String.Format("CopyFolder START From:'{0}' To:'{1}'"
                , sourceDirectory, targetDirectory));

            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
            Info?.Invoke(this, String.Format("CopyFolder DONE From:'{0}' To:'{1}'"
                , sourceDirectory, targetDirectory));
        }

        /// <summary>
        /// Zips a folder to a target zipfile (name must be given ending in .zip)
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="targetDirectory"></param>
        public void ZipFolderToTarget(string folder, string targetDirectory)
        {
            Info?.Invoke(this, String.Format("ZIP_Folder START From:'{0}' To:'{1}'"
                , folder, targetDirectory));

            DirectoryInfo SourceFolder = new DirectoryInfo(folder);
            DirectoryInfo Target = new DirectoryInfo(targetDirectory);
            ZipProccess(SourceFolder, Target);

            Info?.Invoke(this, String.Format("ZIP_Folder DONE From:'{0}' To:'{1}'"
                , folder, targetDirectory));
        }
        private bool ZipProccess(DirectoryInfo folder, DirectoryInfo targetDirectory)
        {
            //open target directory
            try
            {
                //if zip already exists, delete it and redo the process
                if (File.Exists(targetDirectory.FullName))
                    DeleteDirectory(targetDirectory.FullName);

                //check if the directory is valid
                if (!Directory.Exists(folder.FullName)) 
                    throw new ArgumentException($"Filepath {folder} is invalid", "folder");
                
                //zipping processs
                ZipFile.CreateFromDirectory(folder.FullName, targetDirectory.FullName);
            }
            catch (Exception e)
            {
                Info?.Invoke(this, String.Format("Exception found: {0}", e));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Unzip given zipPath to a directory (creates a new directory if it doens exist)
        /// </summary>
        /// <param name="zipPath"></param>
        /// <param name="extractDirectory"></param>
        public void UnzipTargetDirectory(string zipPath, string extractDirectory)
        {
            Info?.Invoke(this, String.Format("Un-Zip START from:'{0}' to:'{1}'"
                , zipPath, extractDirectory));

            DirectoryInfo source = new DirectoryInfo(zipPath);
            DirectoryInfo target = new DirectoryInfo(extractDirectory);

            UnZipProccess(source, target);

            Info?.Invoke(this, String.Format("Un-Zip DONE from:'{0}' to:'{1}'"
                , zipPath, extractDirectory));
        }
        private bool UnZipProccess(DirectoryInfo zipPath, DirectoryInfo extractDirectory)
        {
            //open target directory
            try
            {
                //if directory already exists, delete it and redo the process
                if (File.Exists(extractDirectory.FullName))
                    DeleteDirectory(extractDirectory.FullName);

                //NOTE: the method throws exception when zipath is invalid
                ZipFile.ExtractToDirectory(zipPath.FullName, extractDirectory.FullName);
            }
            catch (Exception e)
            {
                Info?.Invoke(this, String.Format("Exception found: {0}", e));
                return false;
            }
            return true;
        }

        /// <summary>
        /// Create to be used as recursive function to copy all files and sub folders files 
        /// </summary>
        /// <param name="source">Path from where the file will be copied</param>
        /// <param name="target">Path where to copy the files</param>
        public void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Info?.Invoke(this, String.Format(@"CopyFile START from:{0} to:{1}"
                    ,target.FullName, fi.Name));

                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);

                Info?.Invoke(this, String.Format(@"CopyFile DONE from:{0} to:{1}"
                    , target.FullName, fi.Name));
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                Info?.Invoke(this, String.Format(@"CreateNewRepo START Dir:{0} "
                    , diSourceSubDir.Name));

                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);

                Info?.Invoke(this, String.Format(@"CreateNewRepo DONE Dir:{0} "
                    , diSourceSubDir.Name));

                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        /// <summary>
        /// Can throw IOException when try to access unreachable Folder on the SVN 
        /// Can throw SystemException when a file can't be write or overwrited on local drive
        /// </summary>
        /// <param name="from">The link to copy data from</param>
        /// <param name="to">the location on driv where to copy</param>
        public void CopyFilesFromSVN(string from, string to)
        {
            Info?.Invoke(this, String.Format("CopySvnFile START From:'{0}' To: '{1}'", from, to));
            SvnClient myClient = new SvnClient();
            try
            {
                myClient.Export(SvnTarget.FromString(from), to,
                    new SvnExportArgs { Overwrite = true });
            }
            catch (SharpSvn.SvnRepositoryIOException iOException)
            {
                throw new IOException(String.Format("{0}", iOException), iOException);
            }
            catch (SharpSvn.SvnSystemException sysException)
            {
                throw new SystemException(string.Format("{0}", sysException), sysException);
            }
            Info?.Invoke(this, String.Format("CopySvnFile DONE From:'{0}' To: '{1}'", from, to));
        }

        /// <summary>
        /// deletes target Directory
        /// </summary>
        /// <param name="target_dir"></param>
        public void DeleteDirectory(string target_dir)
        {
            Info?.Invoke(this, $"DeleteDirectory START at:{target_dir}");
            //List file in the repository to clean
            string[] files = Directory.GetFiles(target_dir);
            string[] dirs = Directory.GetDirectories(target_dir);

            //Set Normal right (Write, read, delete) to all elements and delete them
            foreach (string file in files)
            {
                try
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }catch (Exception e)
                {
                    Info?.Invoke(this, $"Set the full user right for '{file}' cause of " +
                        $"following exception: {e}");
                    //Add to currentDirectory Write access
                    DirectoryInfo dirInfo = new DirectoryInfo(Path.GetDirectoryName(file));
                    DirectorySecurity dSecurity = dirInfo.GetAccessControl();
                    // Add the FileSystemAccessRule to the security settings. 
                    dSecurity.AddAccessRule(
                        new FileSystemAccessRule(
                            new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                            FileSystemRights.FullControl,
                            InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                            PropagationFlags.InheritOnly, AccessControlType.Allow));
                    dSecurity.AddAccessRule(
                        new FileSystemAccessRule(
                            WindowsIdentity.GetCurrent().Name,
                            FileSystemRights.FullControl,
                            InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                            PropagationFlags.InheritOnly, AccessControlType.Allow));
                    // Set the access control
                    dirInfo.SetAccessControl(dSecurity);

                    //Remove the file readOnly flag
                    var fileInfo = new FileInfo(file);
                    fileInfo.IsReadOnly = false;
                    fileInfo.Attributes = FileAttributes.Normal;
                    fileInfo.Delete();
                }
            }

            //finally delete the sub folder in this local folder (recursive call)
            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }
            //And delete 
            Directory.Delete(target_dir, false);
            Info?.Invoke(this, $"DeleteDirectory DONE at:{target_dir}");
        }

        /// <summary>
        /// Will throw an IOException if folder is not deletable
        /// </summary>
        /// <param name="path"></param>
        public void DeleteFolder(String path)
        {
            Info?.Invoke(this, String.Format("FolderDeletion START. Location: '{0}'", path));
            
            bool restart = true;
            int attempt = 2;
            do
                try
                {
                    DeleteDirectory(path);
                    restart = false;
                }
                catch (Exception e)
                {
                    attempt--;
                    Info?.Invoke(this, $"Can not delete folder at {path}.\nError: {e}");

                    if (attempt < 1) throw e;
                    else System.Threading.Thread.Sleep(300);
                }
            while (restart);
            Info?.Invoke(this, String.Format("FolderDeletion DONE. Location: '{0}'", path));
        }


        /// <summary>
        /// Clones the current project and set ups a branch
        /// </summary>
        /// <param name="localRepo"></param>
        /// <param name="onlineRepository"></param>
        /// <param name="wantedBranch"></param>
        /// <param name="password"></param>
        /// <param name="username"></param>
        public void CloneProjectAndSetUpBranche(
           string localRepo,
           string onlineRepository,
           string wantedBranch,
           string password,
           string username)
        {
            if (Directory.Exists(localRepo))
            {
                Info?.Invoke(this, $"Directory at path '{localRepo}' was detected and will be deleted.");
                DeleteDirectory(localRepo);
            }

            var CloneOptions = new CloneOptions
            {
                CredentialsProvider = (_url, _user, _cred) => new UsernamePasswordCredentials
                {
                    Username = username,
                    Password = password
                }
            };

            Repository.Clone(onlineRepository, localRepo, CloneOptions);
            using (var currentRepo = new Repository(localRepo))
            {
                //If branch not local, need to be create 
                if (currentRepo.Branches[wantedBranch].IsRemote)
                {
                    Branch newLocalBranch = currentRepo.CreateBranch(wantedBranch,
                        currentRepo.Branches[wantedBranch].Tip);

                    // Make the local branch track the upstream one
                    currentRepo.Branches.Update(newLocalBranch,
                         b => b.TrackedBranch = currentRepo.Branches[wantedBranch].CanonicalName);
                }

                Commands.Checkout(currentRepo, currentRepo.Branches[wantedBranch]);
            }
        }

    }
}
