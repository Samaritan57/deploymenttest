﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Serialization; 
using System.IO; 

namespace WorkAreaCreatorLib.Configs
{
    public class BaseSerializerDeserializer
    {
        public bool DeserializeXmlToObject<T>(out T Output, string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                using (Stream reader = new FileStream(path, FileMode.Open))
                    Output = (T)serializer.Deserialize(reader);
                return true; 
            }
            catch
            {
                Output = default; 
                return false; 
            }
        }

        public bool SerializeObjetToXml<T>(T toSerialize, string path)
        {
            try
            {
                var dirPath = Path.GetDirectoryName(path); 
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath); 

                XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                using (TextWriter myWritter = new StreamWriter(path))
                {
                    mySerializer.Serialize(myWritter, toSerialize);
                }
                return true;
            }catch (Exception e)
            {
                Console.WriteLine($"Exception in serialize // deserialize process: \n{e}"); 
                return false;
            }
        }

        public EnvironementBuildInstructions DeserializeConfig (string path)
        {

            XmlSerializer serializer = new XmlSerializer(typeof(EnvironementBuildInstructions));
            EnvironementBuildInstructions toReturn; 
            using (Stream reader = new FileStream(path, FileMode.Open))
                toReturn = (EnvironementBuildInstructions)serializer.Deserialize(reader);
            return toReturn; 
        }

        public void SerializeConfig(string path, EnvironementBuildInstructions toSerialize)
        {
            var dirPath = Path.GetDirectoryName(path);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(EnvironementBuildInstructions));
            using (TextWriter myWritter = new StreamWriter(path))
            {
                mySerializer.Serialize(myWritter, toSerialize);
            }
        }
    }
}
