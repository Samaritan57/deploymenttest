﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace WorkAreaCreatorLib.Configs
{
    public enum ParameterValueType
    {
        FixedValue,
        FromGlobal,
        PathCombination
    }

    public class ParametersDef
    {
        public string Value;
        public ParameterValueType ValueType;


        public ParametersDef() {
            this.Value = null; 
            this.ValueType= ParameterValueType.FixedValue;
        }
        public ParametersDef(string value, ParameterValueType valueType)
        {
            this.Value = value;
            this.ValueType = valueType;
        }
        public ParametersDef(ParameterValueType valueType)
        {
            this.Value = null;
            this.ValueType = valueType;
        }

        private string GetParmVal(SerializableDictionary<string, ParametersDef> GlobalParameters, string valueToReplace)
        {
            if (GlobalParameters.ContainsKey(valueToReplace))
            {
                return GlobalParameters[valueToReplace].Value;
            }
            else
                return valueToReplace; 
        }

        public void UpdateValues(SerializableDictionary<string, ParametersDef> globalParameters)
        {
            switch (ValueType)
            {
                case ParameterValueType.FixedValue:
                    break;

                case ParameterValueType.FromGlobal:
                    this.Value = globalParameters[this.Value].Value;
                    break; 

                case ParameterValueType.PathCombination:
                    var PathToMerge = this.Value.Replace(" ", "").Split(';');
                    string resultingPath = "";

                    foreach (var pathVarID in PathToMerge)
                        resultingPath = Path.Combine(resultingPath, 
                            GetParmVal(globalParameters ,pathVarID));
                    this.Value = resultingPath; 
                    break; 

                default: throw new NotImplementedException($"The param type {ValueType} " +
                    $"is not implemented!review your code!");
            }
        }
    }
}
