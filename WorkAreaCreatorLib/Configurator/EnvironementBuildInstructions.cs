﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WorkAreaCreatorLib.Configs
{
    public class EnvironementBuildInstructions
    {
        //Logging
        public event EventHandler<string> Error;
        public event EventHandler<string> Info;

        public List<ExecutableAction> BuildActions;
        public SerializableDictionary<string, ParametersDef> GlobalParameters;

        public EnvironementBuildInstructions()
        {
            this.BuildActions = new List<ExecutableAction>();
            this.GlobalParameters = new SerializableDictionary<string, ParametersDef>(); 
        }

        public EnvironementBuildInstructions UnserializedFromPath(string path)
        {
            Info?.Invoke(this, $"Try to deserialized config at path: '{path}'"); 
            EnvironementBuildInstructions toReturn;
            System.Xml.Serialization.XmlSerializer mySerializer = 
                new System.Xml.Serialization.XmlSerializer(typeof(EnvironementBuildInstructions));

            using (var myFileStream = new FileStream(path, FileMode.Open))
            {
                toReturn = (EnvironementBuildInstructions)mySerializer.Deserialize(myFileStream);
            }

            Info?.Invoke(this, $"Successfully deserialized config at path: '{path}'");
            return toReturn; 
        }

        public bool Serialize(string path)
        {
            try
            {
                Info?.Invoke(this, $"Try to serialized config at path '{path}'");
                System.Xml.Serialization.XmlSerializer writer =
                    new System.Xml.Serialization.XmlSerializer(typeof(EnvironementBuildInstructions));

                if (File.Exists(path)) File.Delete(path);
                FileStream file = File.Create(path);

                writer.Serialize(file, this);
                file.Close();
                Info?.Invoke(this, $"Successfully serialized config at path: '{path}'");
                return true; 
            }catch (Exception e)
            {
                Error?.Invoke(this, $"Error occured when trying to serialise config at path {path}. " +
                    $"\n Error description\n {e}"); 
                return false;    
            }
        }

        public bool BuildEnvironment(IEngineGuiInteraction GuiInteraction)
        {
            //Update global values with their actual val
            foreach (var globalParams in GlobalParameters)
                globalParams.Value.UpdateValues(GlobalParameters);

            //Update in each action the values
            foreach (var action in BuildActions)
                foreach (var param in action.ParametersDef)
                    param.Value.UpdateValues(GlobalParameters);

            //Finally execute the build action
            //Couple action with the GUI
            foreach (var actionToExec in BuildActions)
                actionToExec.Initialized(GuiInteraction);
            bool result = true;

            //Execute the procedure of each action synchrnonously,
            //stop the procedure if result is set to false
            foreach (var actionToExec in BuildActions)
            {
                if (!result) break;
                result = actionToExec.Execute();
            }
            return result;
        }
    }
}
