﻿using System;
using System.Collections.Generic;
using System.IO; 
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkAreaCreatorLib;

namespace WorkAreaCreator
{
    /// <summary>
    /// Main goal of the program is too: 
    ///  - Provide a way to explore the config files via a navigation system 
    ///  - Permit to download the EtFw and build the project as defined in the config
    ///  file by checking
    /// </summary>
    class Program
    {

        static void Main(string[] args)
        {
            string ConfigFilePath = Path.GetFullPath("Configs");

            #region Object Initialisation and coupling 

            NavigationHelper navigate = new NavigationHelper (
                ConfigFilePath, 
                new TextHelper(), 
                new WACreatorController(), 
                new ConsoleBasedEngineGuiInteraction()
                );

            #endregion
            //Synchrone function blocking the console till it exit...
            navigate.Execute(); 
        }
    }

    
}
