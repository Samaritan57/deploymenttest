﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkAreaCreatorLib;

namespace WorkAreaCreator
{
    public class ConsoleBasedEngineGuiInteraction : IEngineGuiInteraction
    {
        private object classLocker = new object();

        // this is  asimple constructor and seems that the initialize method is
        // needed but in the future we implement more things in the contructor than the Initi
        public ConsoleBasedEngineGuiInteraction()
        {
        }

        public void Initialize(WACreatorController creator)
        {
        }

        /// <summary>
        /// Print some text with color on the console
        /// </summary>
        /// <param name="color">Color to use for printed text</param>
        /// <param name="text">The text to write</param>
        public void Notify(LogLevel level, string text)
        {
            lock (classLocker)
            {
                var consoleCurentColor = Console.ForegroundColor;

                switch (level)
                {
                    case LogLevel.INFO:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case LogLevel.WARNING:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        break;
                    case LogLevel.ERROR:
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    case LogLevel.SUCCESS:
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case LogLevel.OPTION:
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    default:
                        break;
                }
                Console.Write(text);
                Console.ForegroundColor = consoleCurentColor;
            }
        }

        public void OnInfo(object caller, string msg)
        {
            Notify(LogLevel.INFO, msg);
        }

        public bool RequestUserToChangePath(string msg, ref string path)
        {
            Notify(LogLevel.OPTION, "Oups, The programm was not able to access one of the file maybe because " +
                "the path was not correct or the file not acessible. Here is the error description: \n");
            Notify(LogLevel.ERROR, msg + "\n");

            Notify(LogLevel.OPTION, "You have two options, you can try to unlock the file manually and press " +
                "'1' to restart the creation process, or you can press 'c' to cancel the creation.\n");

#if DEBUG
            Notify(LogLevel.OPTION, " 1] Ok, Let me give you the path again, " +
                "I will check it's acessible before. ");
            Notify(LogLevel.OPTION, " c] Cancel! *You start walking*. Go see somewhere else, " +
                "I have no idea of what you're talking about potatoe face. HEEEELP! *You start running*");
#else
            Console.WriteLine(" 1] Enter a new path ");
            Console.WriteLine(" c] Cancel creation ");
#endif

            ConsoleKeyInfo userInput;

            do
            {
                Console.WriteLine();
                Console.Write("User Input: ");
                userInput = Console.ReadKey();
                Console.WriteLine();

                if (userInput.KeyChar == '1')
                {
                    Notify(LogLevel.OPTION, "Ok please enter the new path and press enter to validate");
                    Notify(LogLevel.OPTION, "New path: ");
                    path = Console.ReadLine(); 
                    return true;
                }
                if (userInput.KeyChar == 'c')
                    return false;

                Notify(LogLevel.OPTION, "This command is not valid, you have to press '1' or 'c'. ");
            } while (true);
        }

        public bool RequestUserToUnlockFile(string message)
        {
            Notify(LogLevel.OPTION, "Oups, The programm was not able to access one of the file to read " +
                "or overwrite it, see the following exception for more details: \n");
            Notify(LogLevel.ERROR, message + "\n");

            Notify(LogLevel.OPTION, "You have two options, you can try to unlock the file manually and " +
                "press '1' to restart the creation process, or you can press 'c' to cancel the creation.\n");
#if DEBUG

            Notify(LogLevel.OPTION, " 1] Ok, file seems unlock, please retry again ");
            Notify(LogLevel.OPTION, " c] Cancel, get me out of here °_°''' ");
#else
            Console.WriteLine(" 1] Ok, file is unlock, retry again ");
            Console.WriteLine(" c] Cancel the area creation");
#endif

            ConsoleKeyInfo userInput;

            do
            {
                Console.WriteLine();
                Console.Write("User Input: ");
                userInput = Console.ReadKey();
                Console.WriteLine();

                if (userInput.KeyChar == '1')
                    return true;
                if (userInput.KeyChar == 'c')
                    return false;

                Notify(LogLevel.WARNING, "This command is not valid, you have to press '1' or 'c'. ");

            } while (true);


        }

    }
}
