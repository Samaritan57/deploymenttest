﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using WorkAreaCreatorLib;

namespace WorkAreaCreator
{
    public class NavigationHelper
    {
        readonly string BaseDirectory; 
        string currentDirectory; 
        WACreatorController creator;
        IEngineGuiInteraction engineGuiInteraction; 

        TextHelper printerHelper; 

        public NavigationHelper(string configFilePath, TextHelper printerHelper, WACreatorController creator, IEngineGuiInteraction engineGuiInteraction)
        {
            BaseDirectory = configFilePath;
            currentDirectory = BaseDirectory;

            this.printerHelper = printerHelper;
            this.creator = creator;
            this.engineGuiInteraction = engineGuiInteraction; 
        }

        public void Execute()
        {
            currentDirectory = BaseDirectory;

            printerHelper.SoftInfo();
            Console.WriteLine("Press a key to enter in the soft 'Now or Never Now'. :p");
            Console.ReadKey();

            string UserInput;
            do
            {
                Console.Clear();

                var files = Directory.GetFiles(Path.GetFullPath(currentDirectory), @"*.xml");
                var directories = Directory.GetDirectories(currentDirectory);

                //Vae used to display the place were to navigate and the openable ConfigFile. 
                List<Tuple<int, string>> DirectoriesOptions = new List<Tuple<int, string>>();
                List<Tuple<int, string>> filesOptions = new List<Tuple<int, string>>();

                int count = 0;
                foreach (var dir in directories)
                {
                    DirectoriesOptions.Add(new Tuple<int, string>(count, dir));
                    count++;
                }
                foreach (var file in files)
                {
                    count++;
                }

                printerHelper.PrintNavigationList(
                    files,
                    directories,
                    Path.GetFullPath(currentDirectory),
                    !Path.GetFullPath(currentDirectory).Equals(Path.GetFullPath(BaseDirectory)));

                UserInput = Console.ReadLine();
                Console.WriteLine("\n\n");

                //Start logical according user selection
                switch (UserInput)
                {
                    case "quit"://Byebye cmd
                        Console.Write("Good bye!");
                        for (int I = 0; I < 10; I++)
                        {
                            System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(120));
                            Console.Write(" ..");
                        }
                        break;

                    case "b":
                        if (currentDirectory != BaseDirectory)
                            currentDirectory = Path.GetDirectoryName(Path.GetFullPath(currentDirectory));
                        else
                        {
                            Console.WriteLine("You can't go back in the repo cause you're already in the Root... Press a key to continue.");
                            Console.ReadKey();
                        }
                        break;

                    case "h":
                        currentDirectory = BaseDirectory;
                        break;

                    default:
                        int response;
                        try
                        {
                            response = int.Parse(UserInput);
                            if (DirectoriesOptions.Any(x => x.Item1.Equals(response)))
                            {
                                currentDirectory = Path.GetFullPath(
                                    DirectoriesOptions.Where(x => x.Item1.Equals(response))
                                    .First().Item2);
                            }
                            break;

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(String.Format("The sequence '{0}' is not recognised has a valid option..." +
                                "\n exception details:{1}", UserInput, e));
                            break;
                        }

                }
            } while (!UserInput.Equals("quit"));
        }

        private bool LoadConfigFile(string path)
        {
            return true;
        }

        /// <summary>
        /// Return false if an exception is occuring or if it's going back to base repo...
        /// </summary>
        /// <param name="path"></param>
        /// <param name="files"></param>
        /// <param name="folder"></param>
        /// <returns></returns>
        private bool Explore(string path, out string[] files, out string[] folder)
        {
            if (Directory.Exists(path))
            {

                try
                {

                    folder = Directory.GetDirectories(path);
                    files = Directory.GetFiles(path);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error occured when trying to access too {path}." +
                        $"\nException details:" +
                        $"\n{e}");
                    files = new string[0];
                    folder = new string[0];
                    return false;
                }
            }
            else
            {
                Console.WriteLine($"Error occured when trying to acess the path {path}. Path will be reset to {Path.GetFullPath(BaseDirectory)}. Press a key to continue.");
                Console.ReadKey();
                currentDirectory = BaseDirectory;
                Explore(currentDirectory, out files, out folder);
                return false;
            }

        }



    }
}
