﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkAreaCreator
{
    public class TextHelper
    {
        public void SoftInfo()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            StringBuilder toWrite= new StringBuilder();
            toWrite.AppendLine(@"#########################################################");
            toWrite.AppendLine(@"#               W..ork A..rea C..reator                ##");
            toWrite.AppendLine(@"#      #########################################       ##");
            toWrite.AppendLine(@"#      # Roman Berner Industrieelektronik GmbH #       ##");
            toWrite.AppendLine(@"#      # Roemerstrasse 18    D-71296 Heimsheim #       ##");
            toWrite.AppendLine(@"#      #########################################       ##");
            toWrite.AppendLine(@"#      -------------------------------------           ##");
            toWrite.AppendLine(@"#      #########################################       ##");
            toWrite.AppendLine(@"#      # Problem with the soft?                #       ##");
            toWrite.AppendLine(@"#      #  - Contact: Alexandre Nau             #       ##");
            toWrite.AppendLine(@"#      #  - Mail: alexandre.nau@rbi-online.de  #       ##");
            toWrite.AppendLine(@"#      #  - Tel: +491706931952                 #       ##");
            toWrite.AppendLine(@"#      #########################################       ##");
            toWrite.AppendLine(@"#########################################################");
            Console.WriteLine(toWrite.ToString());
        }

        /// <summary>
        /// Main goal is to display to the user in wich folder he is currently navigating. 
        /// </summary>
        /// <param name="FileList">Current list of folder in this folder</param>
        /// <param name="folderName">The current folder name </param>
        /// <param name="isNotRoot">parameter permitting to know if the user can go back somewhere </param>
        public void PrintNavigationList(string[] fileList, string[] folderList, string folderName, bool isNotRoot = true)
        {

            #region header + instruction in red
            Console.ForegroundColor = ConsoleColor.Red;
            StringBuilder toWrite = new StringBuilder();
            toWrite.AppendLine(@"##########################################################");
            toWrite.AppendLine(@"##                         W.A.C                        ##");
            toWrite.AppendLine(@"## Please navigate and find an environment to generate  ##");
            toWrite.AppendLine(@"##########################################################");
            toWrite.AppendLine();
            toWrite.AppendLine();

            Console.Write(toWrite.ToString());
            toWrite = new StringBuilder();

            #endregion

            #region List of possible entry + go back option in white 

            toWrite.AppendLine($" Write an entry and press enter to navigate somewhere.");
            toWrite.AppendLine();
            toWrite.AppendLine("   - enter 'quit' to resume the application");
            if (isNotRoot)
            {
                toWrite.AppendLine("   - enter'b' to go one folder back");
                toWrite.AppendLine("   - 'h' go back on root ");
            }
            toWrite.AppendLine();

            toWrite.AppendLine($" Current Path:\n    {folderName}");
            toWrite.AppendLine();

            int positionInList = 0;
            foreach (var navigationOption in folderList)
            {

                toWrite.AppendLine($"   - '{positionInList}' Folder: {Path.GetFileName(navigationOption)}'");
                positionInList++;
            }

            foreach (var ConfigOption in fileList)
            {

                toWrite.AppendLine($"   - '{positionInList}' Config: {Path.GetFileName(ConfigOption)}");
                positionInList++;
            }

            toWrite.AppendLine();
            toWrite.Append("## User Input: ");

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(toWrite.ToString());
            #endregion

        }

    }
}
